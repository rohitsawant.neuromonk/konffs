import 'dart:developer';

import 'package:another_flushbar/flushbar.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
// import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendorapp/Screens/Onboarding/welcome_screen.dart';

import '../Models/model_provider.dart';
import '../Utils/route_names.dart';
import '../Widgets/widget_alert_dialog.dart';
import 'iconstant.dart';

class HelperFunction {
  // static Random random =  Random();
  // static int? randomNumber;

  // Set App language
  static setLanguage(BuildContext context, String lang) {
    final provider = Provider.of<ModelProvider>(context, listen: false);
    provider.setLanguage = Locale(lang);
  }

  static showCommonDialog(BuildContext context,
      {required Function positiveButton,
      required Function no,
      required String title,
      required Widget content}) {
    showDialog(
        context: context,
        builder: (context) {
          return WidgetAlertDialog(
              title: title,
              content: content,
              positiveButton: positiveButton,
              no: no);
        });
  }

  static handleResponseErrorMessage(
      {required var apiResponse, required BuildContext context}) {
    if (apiResponse != null) {
      return HelperFunction.showFlushbarError(
          context, apiResponse['message'] ?? "Something went wrong");
    }
    return null;
  }

  static getCurrentScreenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static getCurrentScreenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static Future<String> getuserToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString(IConstant.userToken) ?? '';
  }

  static Future<int> getuserId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt(IConstant.userId) ?? -1;
  }
   static Future<int> getBusinessId() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getInt(IConstant.businessId) ?? -1;
  }

  static Future<bool> getuserIsLoggedIn() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      // log("${preferences.getBool(IConstant.isLogin) ?? false}");
      return preferences.getBool(IConstant.isLogin) ?? false;
    } catch (e) {
      //  print('error for getting token $e');
      return false;
    }
  }

  static Future<String> getRole() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      log("${preferences.getBool(IConstant.userRole) ?? ''}");
      return preferences.getString(IConstant.userRole) ?? '';
    } catch (e) {
      //  print('error for getting token $e');
      return '';
    }
  }

  static Future<String> getUserDetails() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      log("${preferences.getBool(IConstant.userMobileNo) ?? ''}");
      return preferences.getString(IConstant.userMobileNo) ?? '';
    } catch (e) {
      //  print('error for getting token $e');
      return '';
    }
  }

  static Future<String> getUserDetailsEmail() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      log("${preferences.getBool(IConstant.userEmailId) ?? ''}");
      return preferences.getString(IConstant.userEmailId) ?? '';
    } catch (e) {
      //  print('error for getting token $e');
      return '';
    }
  }

  static Future<String> getuserMobileNo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString(IConstant.userMobileNo) ?? "NA";
  }

  static Future<String> getuserName() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString(IConstant.userName) ?? "NA";
  }

  static showFlushbarSuccess(BuildContext context, String msg) {
    return Flushbar(
      margin: const EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(8),
      backgroundColor: Colors.green,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      duration: const Duration(seconds: 3),
    )..show(context);
  }

  static showFlushbarError(BuildContext context, String msg) {
    return Flushbar(
      margin: const EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(8),
      backgroundColor: Colors.red,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      duration: const Duration(seconds: 3),
    )..show(context);
  }

  static comingSoon(BuildContext context) {
    return showFlushbarSuccess(context, "Coming soon!!!");
  }

  static hideKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  // static logout(BuildContext buildContext) async {
  //   SharedPreferences preferences = await SharedPreferences.getInstance();
  //   preferences.clear().then((value) {
  //     Navigator.of(buildContext)
  //         .pushNamedAndRemoveUntil(toRoute(routeLogOut), (route) => false);
  //   });
  // }

  static assignDataToPrefs(apiData, {bool? isLogin}) async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      if (apiData is Map ? apiData['token'].runtimeType == String : false) {
        var userDetails = apiData['userData'] ?? {};
        var business = apiData['business_details'] ?? {};
        await preferences.setInt(IConstant.businessId, business['id'] ?? 0);


        await preferences.setString(
            IConstant.BusinessName, business['company_name'] ?? 'NA');

        // log("${business['company_name']}");
        await preferences.setInt(IConstant.userId, userDetails['id'] ?? 0);
        await preferences.setString(
            IConstant.userName, userDetails['first_name'] ?? 'NA');

        await preferences.setString(
            IConstant.userLastName, userDetails['last_name'] ?? 'NA');
        await preferences.setString(
            IConstant.userEmailId, userDetails['email'] ?? 'NA');

        await preferences.setString(
            IConstant.userMobileNo, userDetails['mobile_number'] ?? "NA");
        await preferences.setString(
            IConstant.userContryCode, userDetails['country_code'] ?? "NA");
        await preferences.setString(IConstant.userToken, apiData['token']);
        await preferences.setBool(IConstant.isLogin, isLogin ?? true);
      }
    } catch (e) {
      // print('storing error $e');
    }
  }

  static checkNullAndAssignDataToPrefs(
      {var apiData, String? iConstantString}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    if (apiData == null) {
      preferences.setBool(iConstantString!, false);
    } else {
      preferences.setBool(
          iConstantString!, apiData.toString() == "0" ? false : true);
    }
  }

  static String dateFormat(String format, DateTime dateTime) {
    return DateFormat(format).format(dateTime);
  }

  static String dateStringFromat(String format, String dateTime) {
    return DateFormat(format).format(DateTime.parse(dateTime)).toString();
  }

  // static void makePhoneCall(String mobileNo) {
  //   try {
  //     launchUrl(Uri.parse("tel:$mobileNo"));
  //   } catch (exception) {
  //     return;
  //   }
  // }

  // static void sendMail(String mailId) async {
  //   try {
  //     launchUrl(Uri.parse("mailto:$mailId"));
  //   } catch (exception) {
  //     return;
  //   }
  // }

  static String? encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((e) =>
            '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }

  static pageTransitionType() {
    return PageTransitionType.rightToLeft;
  }

  static String capitalizeFirstLetter({required String text}) {
    return text[0].toUpperCase() + text.substring(1).toLowerCase();
  }

  static Future<DateTime?> openDatePicker(BuildContext context,
      {DateTime? initialDate}) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: initialDate ?? DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != initialDate) {
      return picked;
    }
    return null;
  }

  static MaterialColor getMaterialColor(Color color, int value) {
    return MaterialColor(color.hashCode, <int, Color>{
      50: color.withOpacity(0.05),
      100: color.withOpacity(0.1),
      200: color.withOpacity(0.2),
      300: color.withOpacity(0.3),
      400: color.withOpacity(0.4),
      500: color.withOpacity(0.5),
      600: color.withOpacity(0.6),
      700: color.withOpacity(0.7),
      800: color.withOpacity(0.8),
      900: color.withOpacity(0.9),
    });
  }

  static formatNumber(int num) {
    var formattedNumber = NumberFormat.compactCurrency(
      decimalDigits: 2,
      locale: 'en_AE',
      symbol: '',
    ).format(num);
    return formattedNumber;
  }

  static convertDate(String strDate) {
    if (strDate.isEmpty) {
      return 'Undefined';
    }
    DateTime date = DateTime.parse(strDate);
    return DateFormat('MMM dd, y').format(date);
  }

  // static showBottomSheet({
  //   required BuildContext context,
  //   required WidgetBottomSheet child,
  // }) {
  //   showModalBottomSheet<void>(
  //     backgroundColor: Colors.transparent,
  //     context: context,
  //     builder: (BuildContext context) {
  //       return child;
  //     },
  //   );
  // }

  // static Future<File?> cropImage(
  //     BuildContext context,
  //     File file,
  //     ) async {
  //   File? compressedImage;
  //   await ImageCropper().cropImage(
  //     sourcePath: file.path,
  //     maxHeight: 100,
  //     aspectRatioPresets: [CropAspectRatioPreset.ratio16x9],
  //     uiSettings: [
  //       AndroidUiSettings(
  //           toolbarTitle: 'Crop Image',
  //           toolbarColor: ThemeColors.primary,
  //           toolbarWidgetColor: Colors.white,
  //           lockAspectRatio: true,
  //           hideBottomControls: true)
  //     ],
  //   ).then((cropped) async {
  //     if (cropped != null) {
  //       randomNumber = random.nextInt(100);
  //       File resultedFile = File(cropped.path);
  //       compressedImage = resultedFile;
  //       return compressedImage;
  //     }
  //   });
  //   return compressedImage;
  // }

  // static Future<File?> getFileData(
  //     BuildContext context,
  //     ) async {
  //   File? result;
  //   await ImagePicker()
  //       .pickImage(source: ImageSource.gallery)
  //       .then((selectedFile) async {
  //     if (selectedFile != null) {
  //       await cropImage(
  //         context,
  //         File(.path),
  //       ).then((value) {
  //         result = value;
  //         return value;
  //       });
  //     }
  //   });
  //   return result;
  // }
}

// import 'dart:io';
// import 'dart:math';
// import 'dart:developer' as dev;
//
// import 'package:another_flushbar/flushbar.dart';
// import 'package:flutter/material.dart';
// // import 'package:image_cropper/image_cropper.dart';
// // import 'package:image_picker/image_picker.dart';
// import 'package:intl/intl.dart';
// import 'package:page_transition/page_transition.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// // import 'package:shared_preferences/shared_preferences.dart';
// // import 'package:url_launcher/url_launcher.dart';
// import '../models/model_provider.dart';
// import '../routes/routes.dart';
// import '../widgets/widget_alert_dialog.dart';
// import '../widgets/widget_bottom_sheet.dart';
// import 'iconstant.dart';
//
// class HelperFunction {
//   static Random random =  Random();
//   static int? randomNumber;
//
//   // Set App language
//   static setLanguage(BuildContext context, String lang) {
//     final provider = Provider.of<ModelProvider>(context, listen: false);
//     provider.setLanguage = Locale(lang);
//   }
//
//   static showCommonDialog(BuildContext context,
//       {required Function positiveButton,
//         required Function no,
//         required String title,
//         required Widget content}) {
//     showDialog(
//         context: context,
//         builder: (context) {
//           return WidgetAlertDialog(
//               title: title,
//               content: content,
//               positiveButton: positiveButton,
//               no: no);
//         });
//   }
//
//   static handleResponseErrorMessage(
//       {required var apiResponse, required BuildContext context}) {
//     if (apiResponse != null) {
//       return HelperFunction.showFlushbarError(
//           context, apiResponse['message'] ?? "Something went wrong");
//     }
//     return null;
//   }
//
//   static getCurrentScreenHeight(BuildContext context) {
//     return MediaQuery.of(context).size.height;
//   }
//
//   static getCurrentScreenWidth(BuildContext context) {
//     return MediaQuery.of(context).size.width;
//   }
//
//   // static Future<String> getuserToken() async {
//   //   SharedPreferences preferences = await SharedPreferences.getInstance();
//   //   return preferences.getString(IConstant.userToken) ?? '';
//   // }
//
//   // static Future<int> getuserId() async {
//   //   SharedPreferences preferences = await SharedPreferences.getInstance();
//   //   return preferences.getInt(IConstant.userId) ?? -1;
//   // }
//
//   // static Future<bool> getuserIsLoggedIn() async {
//   //   try {
//
//   //   SharedPreferences preferences = await SharedPreferences.getInstance();
//   //   return preferences.getBool(IConstant.isLogin) ?? false;
//   //   } catch (e) {
//   //   //  print('error for getting token $e');
//   //     return false;
//   //   }
//   // }
//
//   // static Future<String> getuserMobileNo() async {
//   //   SharedPreferences preferences = await SharedPreferences.getInstance();
//   //   return preferences.getString(IConstant.userMobileNo) ?? "NA";
//   // }
//
//   // static Future<String> getuserName() async {
//   //   SharedPreferences preferences = await SharedPreferences.getInstance();
//   //   return preferences.getString(IConstant.userName) ?? "NA";
//   // }
//
//   static showFlushbarSuccess(BuildContext context, String msg) {
//     return Flushbar(
//       margin: const EdgeInsets.all(8),
//       borderRadius: BorderRadius.circular(8),
//       backgroundColor: Colors.green,
//       flushbarPosition: FlushbarPosition.TOP,
//       message: msg,
//       duration: const Duration(seconds: 3),
//     )..show(context);
//   }
//
//   static showFlushbarError(BuildContext context, String msg) {
//     return Flushbar(
//       margin: const EdgeInsets.all(8),
//       borderRadius: BorderRadius.circular(8),
//       backgroundColor: Colors.red,
//       flushbarPosition: FlushbarPosition.TOP,
//       message: msg,
//       duration: const Duration(seconds: 3),
//     )..show(context);
//   }
//
//   static comingSoon(BuildContext context) {
//     return showFlushbarSuccess(context, "Coming soon!!!");
//   }
//
//   static hideKeyboard(BuildContext context) {
//     FocusScopeNode currentFocus = FocusScope.of(context);
//     if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
//       FocusManager.instance.primaryFocus!.unfocus();
//     }
//   }
//
logout(BuildContext context) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.clear().then((value) {
    Get.offAllNamed(RouteNames.WELCOME_SCREEN);
    // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>const WelcomeScreen()), (route) => false);
    // Navigator.of(buildContext)
    // .pushNamedAndRemoveUntil(routes.routeLogOut, (route) => false);
  });
}
//
//   static assignDataToPrefs(apiData, {bool? isLogin}) async {
//     try {
//     // print('its comming out ${apiData is Map}');
//
//     SharedPreferences preferences = await SharedPreferences.getInstance();
//     // print(isLogin.toString());
//     if (apiData is Map?apiData['token'].runtimeType == String:false) {
//       dev.log('$apiData');
//     // print('its comming');
//     var userDetails = apiData.runtimeType == Map? apiData['user_data'] ?? {}:{};
//    await preferences.setInt(IConstant.userId, userDetails['user_id'] ?? 0);
//    await preferences.setString(IConstant.userName, userDetails['driver_name'] ?? '');
//    await preferences.setString(IConstant.userEmailId, userDetails['email'] ?? 'NA');
//    await preferences.setString(
//         IConstant.userMobileNo, userDetails['mobile_no'] ?? "NA");
//    await preferences.setString(
//         IConstant.userAddress, userDetails['address'] ?? "NA");
//    await preferences.setString(IConstant.userRole, "${userDetails['role'] ?? '1'}");
//    await preferences.setString(
//         IConstant.userStatus, "${userDetails['status'] ?? '1'}");
//     await  preferences.setString(IConstant.userToken, apiData['token']);
//      await preferences.setBool(IConstant.isLogin, isLogin ?? true);
//     }
//     } catch (e) {
//       // print('storing error $e');
//     }
//   }
//
//   // static checkNullAndAssignDataToPrefs(
//   //     {var apiData, String? iConstantString}) async {
//   //   SharedPreferences preferences = await SharedPreferences.getInstance();
//
//   //   if (apiData == null) {
//   //     preferences.setBool(iConstantString!, false);
//   //   } else {
//   //     preferences.setBool(
//   //         iConstantString!, apiData.toString() == "0" ? false : true);
//   //   }
//   // }
//
//   static String dateFormat(String format, DateTime dateTime) {
//     return DateFormat(format).format(dateTime);
//   }
//
//   static String dateStringFromat(String format, String dateTime) {
//     return DateFormat(format).format(DateTime.parse(dateTime)).toString();
//   }
//
//   // static void makePhoneCall(String mobileNo) {
//   //   try {
//   //     urla
//   //     launchUrl(Uri.parse("tel:$mobileNo"));
//   //   } catch (exception) {
//   //     return;
//   //   }
//   // }
//
//   // static void sendMail(String mailId) async {
//   //   try {
//   //     launchUrl(Uri.parse("mailto:$mailId"));
//   //   } catch (exception) {
//   //     return;
//   //   }
//   // }
//
//   static String? encodeQueryParameters(Map<String, String> params) {
//     return params.entries
//         .map((e) =>
//     '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
//         .join('&');
//   }
//
//   static pageTransitionType() {
//     return PageTransitionType.rightToLeft;
//   }
//
//   static String capitalizeFirstLetter({required String text}) {
//     return text[0].toUpperCase() + text.substring(1).toLowerCase();
//   }
//
//   static MaterialColor getMaterialColor(Color color, int value) {
//     return MaterialColor(color.hashCode, <int, Color>{
//       50: color.withOpacity(0.05),
//       100: color.withOpacity(0.1),
//       200: color.withOpacity(0.2),
//       300: color.withOpacity(0.3),
//       400: color.withOpacity(0.4),
//       500: color.withOpacity(0.5),
//       600: color.withOpacity(0.6),
//       700: color.withOpacity(0.7),
//       800: color.withOpacity(0.8),
//       900: color.withOpacity(0.9),
//     });
//   }
//
//   static formatNumber(int num) {
//     var formattedNumber = NumberFormat.compactCurrency(
//       decimalDigits: 2,
//       locale: 'en_AE',
//       symbol: '',
//     ).format(num);
//     return formattedNumber;
//   }
//
//   static convertDate(String strDate) {
//     if (strDate.isEmpty) {
//       return 'Undefined';
//     }
//     DateTime date = DateTime.parse(strDate);
//     return DateFormat('MMM dd, y').format(date);
//   }
//
//   static showBottomSheet({
//     required BuildContext context,
//     required WidgetBottomSheet child,
//   }) {
//     showModalBottomSheet<void>(
//       backgroundColor: Colors.transparent,
//       context: context,
//       builder: (BuildContext context) {
//         return child;
//       },
//     );
//   }
//
// // static Future<File?> cropImage(
// //   BuildContext context,
// //   File file,
// // ) async {
// //   File? compressedImage;
// //   await ImageCropper().cropImage(
// //     sourcePath: file.path,
// //     maxHeight: 100,
// //     aspectRatioPresets: [CropAspectRatioPreset.ratio16x9],
// //     uiSettings: [
// //       AndroidUiSettings(
// //           toolbarTitle: 'Crop Image',
// //           toolbarColor: ThemeColors.primary,
// //           toolbarWidgetColor: Colors.white,
// //           lockAspectRatio: true,
// //           hideBottomControls: true)
// //     ],
// //   ).then((cropped) async {
// //     if (cropped != null) {
// //       randomNumber = random.nextInt(100);
// //       File resultedFile = File(cropped.path);
// //       compressedImage = resultedFile;
// //       return compressedImage;
// //     }
// //   });
// //   return compressedImage;
// // }
//
// // static Future<File?> getFileData(
// //   BuildContext context,
// // ) async {
// //   File? result;
// //   await ImagePicker()
// //       .pickImage(source: ImageSource.gallery)
// //       .then((selectedFile) async {
// //     if (selectedFile != null) {
// //       await cropImage(
// //         context,
// //         File(selectedFile.path),
// //       ).then((value) {
// //         result = value;
// //         return value;
// //       });
// //     }
// //   });
// //   return result;
// // }
// }

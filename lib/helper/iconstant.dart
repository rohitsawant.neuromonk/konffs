class IConstant {
  static String regExMobileNumber = '^[6-9]{1}[0-9]{9}';
  static String regExEmail =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
  static int initialTimeToResendOTP = 60;
  static String rupees = '₹';
  // Login
  static String isLogin = "registerUser";
  // SignUp
    static String isSignUp = "registerUser";
  static String userMobileNo = "user_mobile_no";
   static String userContryCode = "user_contry_code";
  static String userName = 'user_name';
  static String userEmailId = 'user_email_id';
  static String userPassword = "password";
  static String userConfPassword = "password_confirmation";
  static String userLastName = "last_name";
  static String BusinessName= "business_nsme";
  static String userId = "user_id";
   static String businessId = "businessId";
  static String userToken = "user_token";
  static String userAddress = 'user_address';
  static String userRole = 'user_role';
  static String userStatus = 'user_status';
  static String showIntroTutorial = "show_intro_tutorial";
  static String baseImageUrl = "#########";

}


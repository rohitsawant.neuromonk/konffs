import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:vendorapp/Models/offer_model.dart';
import 'package:vendorapp/services/service_api.dart';
import '../Models/sign_up.dart';
import '../Screens/Home/model/offer_model.dart';
import '../models/models.dart';

class ServiceSignUp {
  static signup(BuildContext context, SignUp signUp) async {
    var body = {
      "first_name": signUp.fname,
      "last_name": signUp.lname,
      "email": signUp.email,
      "country_code": signUp.countryCode,
      "mobile_number": signUp.mobileNo,
      "businessDetail": [
        {
          "company_name": signUp.companyname,
          "contact_number": signUp.contactNumber,
          "ratings": signUp.ratings,
          "place_id": signUp.placeId,
          "latitude": signUp.latitude,
          "langitude": signUp.longitude,
          "address_line_1": signUp.addressLine1,
          "address_line_2": signUp.addressLine2,
          "city": signUp.city,
          "state": signUp.state,
          "pincode": signUp.pincode
        }
      ]
    };
    log("$body");
    var response = await ServiceApi.api(
        url: Url.signup,
        type: EnumApiType.post,
        context: context,
        headerWithTokenBool: true,
        body: jsonEncode(body));
    // log("message: $response");
    return response;
  }
}

class ServiceCheckUser {
  static checkUser(BuildContext context, String email) async {
    var body = {
      "email": email,
    };
    // log("$body");
    var response = await ServiceApi.api(
        url: Url.checkUser,
        type: EnumApiType.post,
        context: context,
        headerWithTokenBool: true,
        body: jsonEncode(body));
    // log("message: $response");
    return response;
  }
}

class ServiceAddBusiness {
  static addBusiness(BuildContext context, AddBusiness addBusiness) async {
    var body = {
      "businessDetail": [
        {
          "company_name": addBusiness.companyname,
          "contact_number": addBusiness.contactNumber,
          "ratings": addBusiness.ratings,
          "place_id": addBusiness.placeId,
          "latitude": addBusiness.latitude,
          "langitude": addBusiness.longitude,
          "address_line_1": addBusiness.addressLine1,
          "address_line_2": addBusiness.addressLine2,
          "city": addBusiness.city,
          "state": addBusiness.state,
          "pincode": addBusiness.pincode
        }
      ]
    };

    log("$body");
    var response = await ServiceApi.api(
        url: Url.addBusiness,
        type: EnumApiType.post,
        context: context,
        headerWithTokenBool: true,
        body: jsonEncode(body));
    log("message.12345: $response");
    return response;
  }
}

class ServiceOfferadd {
  static addOffer(BuildContext context, OfferList offerList) async {
    var body = {
      "offer_details": offerList.offerDetails,
      "discount": offerList.discount,
      "business_details_id": offerList.businessDetailsId,
      "category_type": offerList.categoryType,
      "offer_start_date": offerList.offerStartDate,
      "offer_end_date": offerList.offerEndDate,
    };
    log("$body");
    var response = await ServiceApi.api(
        url: Url.addOffer,
        type: EnumApiType.post,
        context: context,
        headerWithTokenBool: true,
        body: jsonEncode(body));
    // log("message: $response");
    return response;
  }
}
class ServiceEditOffers {
  static editOffers(BuildContext context, OfferList offerList,int offerid) async {
    var body = {
   
      "offer_details": offerList.offerDetails,
      "discount": offerList.discount,
      "business_details_id": offerList.businessDetailsId,
      "category_type": offerList.categoryType,
      "offer_start_date": offerList.offerStartDate,
      "offer_end_date": offerList.offerEndDate,
    };
    log("$body");
    var response = await ServiceApi.api(
        url: "${Url.editOffers}?id=${offerid}",
        type: EnumApiType.post,
        context: context,
        headerWithTokenBool: true,
        body: jsonEncode(body));
    // log("message: $response");
    return response;
  }
}

SignUp signUp = SignUp.init();

class ServiceOfferGet {
  static getoffers(BuildContext context, int id) async {

    // log("${body}");
    var response = await ServiceApi.api(
      url: "${Url.getOffers}?business_details_id=${id}",
      type: EnumApiType.get,
      context: context,
      headerWithTokenBool: true,
    );
    return response;
  }
}

Offers offers = Offers.init();

class ServiceOfferDelete {
  static deleteOffer(BuildContext context, int offers) async {
    var body = {
      "id": offers,
    };
    log("${body}");
    var response = await ServiceApi.api(
      url: "${Url.deleteOffer}?id=${offers}",
      type: EnumApiType.delete,
      context: context,
      headerWithTokenBool: true,
      //  body: jsonEncode(response)
    );
    // log("${response.body}");
    return response;
  }
}

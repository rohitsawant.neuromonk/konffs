// ignore_for_file: use_build_context_synchronously

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/cupertino.dart';

import 'package:http/http.dart' as http;
// ignore: depend_on_referenced_packages
import 'package:path/path.dart' as path;
import 'package:provider/provider.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vendorapp/Models/offer_model.dart';

import '../helper/helper_function.dart';
import '../helper/iconstant.dart';
import '../models/model_provider.dart';
import '../models/models.dart';

class Url {
  static const String localUrl =
      "http://knoffs-api.neuromonk.com/api/v1/client/";
  static const String serverUrl =
      "http://knoffs-api.neuromonk.com/api/v1/client/";
  static const String baseUrl = localUrl;
  // Endpoints

  static const String signup = "registerUser";
  static const String checkUser="checkUser";
  static const String addBusiness = "addBusiness";
  static const String addOffer = "addOffers";
   static const String editOffers= "editOffers";
  static const String getOffers = "getAllOffers";
  static const String deleteOffer = "deleteOffersById";
}

class ServiceApi {
  ServiceApi(String email, password);

  static Future api(
      {required String url,
      required EnumApiType type,
      required BuildContext context,
      bool? headerWithTokenBool,
      var body,
      bool? showSuccessFalseMsg}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Map<String, String> headers = {
      'Content-Type': 'application/json',
    };
    Map<String, String> headerWithToken = {
      'Content-Type': 'application/json',
      'Authorization':
          'Bearer ${preferences.getString(IConstant.userToken).toString()}'
    };
    // log(Url.baseUrl + url);
    log("API : ${Url.baseUrl + url}");
    // log("${preferences.getString(IConstant.userToken).toString()}");
    // ignore: avoid_print
    // log(preferences.getString(IConstant.userToken).toString());
    switch (type) {
      case EnumApiType.get:
        {
          try {
            final http.Response response = await http.get(
              Uri.parse(Url.baseUrl + url),
              headers: headerWithTokenBool == true ? headerWithToken : headers,
            );
            if (response.statusCode == 500 ||
                response.statusCode == 501 ||
                response.statusCode == 502 ||
                response.statusCode == 503) {
              // Navigator.pushReplacementNamed(context, Routes.routeServerError);
              return;
            } else {
              // print('$url ${response.statusCode}');
              // log("token : $headerWithToken");
              // log(response.body);
              return checkResponse(response, context,
                  showSuccessFalseMsg: showSuccessFalseMsg == true);
            }
          } on SocketException catch (e) {
            log(e.toString());
            HelperFunction.showFlushbarError(
                context, "Please check internet connection");
            return <String, dynamic>{};
          } catch (e) {
            // print(e.toString());
            HelperFunction.showFlushbarError(context, "Something went wrong");
            return <String, dynamic>{};
          }
        }

      case EnumApiType.post:
        {
          try {
            // log(body.toString());
            final http.Response response = await http.post(
              Uri.parse(Url.baseUrl + url),
              headers: headerWithTokenBool == true ? headerWithToken : headers,
              body: body,
            );
            // log(response.body);

            return checkResponse(response, context,
                showSuccessFalseMsg: showSuccessFalseMsg == true);
          } on SocketException catch (e) {
            debugPrint(e.toString());
            HelperFunction.showFlushbarError(
                context, "Please check internet connection");
            return <String, dynamic>{};
          } catch (e) {
            debugPrint(e.toString());
            // log("${e}");
            HelperFunction.showFlushbarError(context, "Something went wrong");
            return <String, dynamic>{};
          }
        }
      case EnumApiType.put:
        {}
        break;
      case EnumApiType.delete:
        {
          try {
            final http.Response response = await http.delete(
              Uri.parse(Url.baseUrl + url),
              headers: headerWithTokenBool == true ? headerWithToken : headers,
            );
            // log("${response.body}");
            return checkResponse(response, context);
          } on SocketException catch (e) {
            debugPrint(e.toString());
            final provider = Provider.of<ModelProvider>(context);
            provider.setInternetConnection = false;
            // HelperFunction.showFlushbarError(
            //     context, "Please check internet connection");
            return <String, dynamic>{};
          } catch (e) {
            debugPrint(e.toString());
            HelperFunction.showFlushbarError(context, "Something went wrong");
            return <String, dynamic>{};
          }
        }
      default:
        {
          log('Wrong choice dude!!!');
        }
    }
  }

  static Future<Map<String, dynamic>> apiFormData(
      {required String url,
      required EnumApiType type,
      required BuildContext context,
      bool? headerWithTokenBool,
      var body,
      bool? showSuccessFalseMsg}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // Map<String, String> headers = {
    //   'Content-Type': 'multipart/form-data',
    // };
    Map<String, String> headerWithToken = {
      'Authorization':
          'Bearer ${preferences.getString(IConstant.userToken).toString()}'
    };
    // log(Url.baseUrl + url);
    log("API : ${Url.baseUrl + url}");
    // ignore: avoid_print
    // log(preferences.getString(IConstant.userToken).toString());
    switch (type) {
      case EnumApiType.post:
        {
          try {
            var req =
                http.MultipartRequest('POST', Uri.parse(Url.baseUrl + url));
            if (headerWithTokenBool == true) {
              req.headers.addAll(headerWithToken);
            }

            req.fields.addAll(body);
            var res = await req.send();
            final http.Response response = await http.Response.fromStream(res);
            if (response.statusCode == 500 ||
                response.statusCode == 501 ||
                response.statusCode == 502 ||
                response.statusCode == 503) {
              // Navigator.pushReplacementNamed(context, Routes.routeServerError);
              return <String, dynamic>{};
            } else {
              return checkResponse(response, context,
                  showSuccessFalseMsg: showSuccessFalseMsg == true);
            }
          } on SocketException catch (e) {
            log(e.toString());
            HelperFunction.showFlushbarError(
                context, "Please check internet connection");
            return <String, dynamic>{};
          } catch (e) {
            // print(e.toString());
            HelperFunction.showFlushbarError(context, "Something went wrong");
            return <String, dynamic>{};
          }
        }

      case EnumApiType.put:
        {
          try {
            var req =
                http.MultipartRequest('PUT', Uri.parse(Url.baseUrl + url));
            if (headerWithTokenBool == true) {
              req.headers.addAll(headerWithToken);
            }

            req.fields.addAll(body);
            var res = await req.send();
            final http.Response response = await http.Response.fromStream(res);
            return checkResponse(response, context,
                showSuccessFalseMsg: showSuccessFalseMsg == true);
          } on SocketException catch (e) {
            debugPrint(e.toString());
            HelperFunction.showFlushbarError(
                context, "Please check internet connection");
            return <String, dynamic>{};
          } catch (e) {
            debugPrint(e.toString());
            HelperFunction.showFlushbarError(context, "Something went wrong");
            return <String, dynamic>{};
          }
        }
      default:
        {
          log('Wrong choice dude!!!');
          return <String, dynamic>{};
        }
    }
  }

  static Map<String, dynamic> checkResponse(
      http.Response response, BuildContext context,
      {bool? showSuccessFalseMsg}) {
    // log(response.statusCode.toString());
    // log('${response.body}');
    // log(response.body.runtimeType.toString());
    switch (response.statusCode) {
      case 200:
        Map<String, dynamic>? responseJson = json.decode(response.body);
        // log(responseJson.toString());
        if (responseJson != null &&
            (responseJson['success'] == true ||
                responseJson['success'] == 200)) {
          return responseJson;
        }
        return <String, dynamic>{};
      case 403:
        // HelperFunction.logout(context);
        HelperFunction.showFlushbarError(
            context, "Session token expire.Please login again");
        // HelperFunction.logout(context);
        return <String, dynamic>{};
      case 404:
        HelperFunction.showFlushbarError(context, "Server url not found");
        return <String, dynamic>{};
      case 412:
        var responseJson = json.decode(response.body.toString());
        // log(responseJson.toString());
        Map<String, dynamic> data = responseJson != null
            ? responseJson['data'] ?? <String, dynamic>{}
            : <String, dynamic>{};
        String error = '';
        int lastIndex = 0;
        data.forEach((key, value) {
          if (data.length == 1 || lastIndex == data.length - 1) {
            error += value[0];
          } else {
            error += value[0] + "\n";
          }
          lastIndex++;
        });
        HelperFunction.showFlushbarError(context, error);
        return <String, dynamic>{};
      case 413:
      log("${response.body}");
        var responseJson = json.decode(response.body.toString());
        // log(responseJson.toString());
        HelperFunction.showFlushbarError(
            context,
            responseJson != null
                ? responseJson['data'] ?? 'Something went wrong on server'
                : 'Something went wrong on server');
                
        return <String, dynamic>{};
      case 500:
        HelperFunction.showFlushbarError(
            context, "Something went wrong on server");
        return <String, dynamic>{};
      default:
        HelperFunction.showFlushbarError(
            context, "Something went wrong on server");
        return <String, dynamic>{};
    }
  }

  static apiMultipartRegister(String url, Map<String, String> body,
      File profileImageFile, BuildContext context) async {
    var request = http.MultipartRequest('POST', Uri.parse(Url.baseUrl + url));
    var length = await profileImageFile.length();
    var stream = http.ByteStream(Stream.castFrom(profileImageFile.openRead()));
    var pic = http.MultipartFile("profile_photo", stream, length,
        filename: path.basename(profileImageFile.path));
    request.files.add(pic);
    request.fields.addAll(body);
    // log(request.fields.toString());
    return checkResponse(
        await request
            .send()
            .then((response) => http.Response.fromStream(response)),
        context);
  }

  static apiMultipartAddPost(BuildContext context,
      {required String url,
      required Map<String, String> body,
      required File imageFile}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var request = http.MultipartRequest('POST', Uri.parse(Url.baseUrl + url));
    var length = await imageFile.length();
    var stream = http.ByteStream(Stream.castFrom(imageFile.openRead()));
    var pic = http.MultipartFile("image", stream, length,
        filename: path.basename(imageFile.path));
    request.files.add(pic);
    request.headers.addAll({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${preferences.getString(IConstant.userToken)}'
    });
    request.fields.addAll(body);
    return checkResponse(
        await request
            .send()
            .then((response) => http.Response.fromStream(response)),
        context);
  }

  static apiMultipartUpdateProfile(BuildContext context,
      {required String url,
      required Map<String, String> body,
      required File? imageFile}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var request = http.MultipartRequest('POST', Uri.parse(Url.baseUrl + url));
    if (imageFile != null && imageFile.path.isNotEmpty) {
      var length = await imageFile.length();
      var stream = http.ByteStream(Stream.castFrom(imageFile.openRead()));
      var pic = http.MultipartFile("profile_photo", stream, length,
          filename: path.basename(imageFile.path));
      request.files.add(pic);
    }

    request.headers.addAll({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${preferences.getString(IConstant.userToken)}'
    });
    request.fields.addAll(body);
    return checkResponse(
        await request
            .send()
            .then((response) => http.Response.fromStream(response)),
        context);
  }
}

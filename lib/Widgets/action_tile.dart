import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';

class ActionTile extends StatelessWidget {
  final String img;
  final String title;
  final void Function()? onTap;
  const ActionTile(
      {Key? key, required this.img, required this.title, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: InkWell(
        onTap: onTap,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 15),
              child: Image.asset(
                img,
                height: 20,
              ),
            ),
            Text(
              title,
              style: const TextStyle(
                  color: AppColors.darkGrey,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
            const Expanded(child: SizedBox()),
            const Icon(
              Icons.arrow_forward_ios_outlined,
              color: AppColors.darkGrey,
              size: 15,
            )
          ],
        ),
      ),
    );
  }
}

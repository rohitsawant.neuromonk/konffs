import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:map_location_picker/map_location_picker.dart';
import '../Utils/app_colors.dart';
import '../Utils/app_images.dart';
import '../Utils/route_names.dart';

class HomeHeader extends StatelessWidget {
  // final String userName;
  final String address;
  final void Function(String? formattedAddress, String coordinates)
      pickLocation;
  const HomeHeader(
      {Key? key,
      // required this.userName,
      required this.address,
      required this.pickLocation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Text(
            //   "Welcome $userName,",
            //   style: const TextStyle(
            //       color: AppColors.disabledGrey,
            //       fontSize: 12,
            //       fontWeight: FontWeight.w400),
            // ),
            const SizedBox(height: 5),
            GestureDetector(
              onTap: () {
                Get.to(() => MapLocationPicker(
                      apiKey: 'AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c',
                      onSuggestionSelected: (selectedPlace) => pickLocation(
                          selectedPlace?.result.formattedAddress,
                          '${selectedPlace?.result.geometry?.location.lat},${selectedPlace?.result.geometry?.location.lng}'),
                      onNext: (GeocodingResult? result) => pickLocation(
                          result?.formattedAddress,
                          '${result?.geometry.location.lat},${result?.geometry.location.lng}'),
                    ));
              },
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 18.5,
                    backgroundColor: AppColors.green.withOpacity(.35),
                    child: Image.asset(
                     'assets/icons/green_location_icon.png',
                      height: 20,
                    ),
                  ),
                  const SizedBox(width: 7),
                  SizedBox(
                    width: MediaQuery.of(context).size.width /1.6,
                    child: Text(
                      address,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: const TextStyle(
                          color: AppColors.darkGrey,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                   GestureDetector(
                  onTap: () => Get.toNamed(RouteNames.NOTIFICATION_SCREEN),
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      color: AppColors.lightGrey.withOpacity(.5),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    alignment: Alignment.center,
                    child: Image.asset(
                      AppImages.notificationIcon,
                      height: 22,
                    ),
                  ),
                ),
                ],
              ),
            )
          ],
        ),
        // Container(
        //   width: 45,
        //   height: 45,
        //   decoration: const BoxDecoration(
        //     borderRadius: BorderRadius.only(
        //       topLeft: Radius.circular(15),
        //       topRight: Radius.circular(15),
        //       bottomLeft: Radius.circular(15),
        //       bottomRight: Radius.circular(15),
        //     ),
        //     boxShadow: [
        //       BoxShadow(
        //           color: Color.fromRGBO(19, 77, 90, 0.20000000298023224),
        //           offset: Offset(11, 28),
        //           blurRadius: 50)
        //     ],
        //     color: Color.fromRGBO(250, 253, 255, 1),
        //   ),
        //   alignment: Alignment.center,
        //   child: Image.asset(
        //     AppImages.notificationIcon,
        //     height: 23,
        //   ),
        // )
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Utils/app_colors.dart';

class CustomBackButton extends StatelessWidget {
  final Color? color;
  final void Function()? onTap;
  const CustomBackButton({Key? key, this.color, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ?? () => Get.back(),
      child: Container(
        margin: const EdgeInsets.only(top: 20, left: 25),
        height: 45,
        width: 45,
        decoration: BoxDecoration(
            color: color ?? AppColors.lightOrange.withOpacity(.1),
            borderRadius: BorderRadius.circular(15)),
        alignment: const Alignment(.3, 0),
        child: const Icon(
          Icons.arrow_back_ios,
          color: AppColors.darkOrange,
          size: 17,
        ),
      ),
    );
  }
}

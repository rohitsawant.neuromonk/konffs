import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';

class CustomButton2 extends StatelessWidget {
  final String label;
  final Color? color;
  final Icon icon;
  final void Function()? onTap;
  const CustomButton2(
      {Key? key,
      required this.label,
      this.onTap,
      this.color,
      required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 57,
        margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: const LinearGradient(
              begin: Alignment(-0.9752321839332581, 1.1797842702776506e-9),
              end: Alignment(2.8603663881909824e-9, 0.03538257256150246),
              colors: [
                Color.fromRGBO(186, 106, 106, 1),
                Color.fromRGBO(188, 57, 64, 1)
              ]),
        ),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              label,
              style: const TextStyle(
                  color: AppColors.background,
                  fontSize: 14,
                  fontWeight: FontWeight.w700),
            ),
            const SizedBox(width: 20,),
            Icon(
              icon.icon,
              size: 25,
              color: AppColors.background,
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../Utils/app_colors.dart';

class CustomTextField extends StatelessWidget {
  final String hintText;
  final TextEditingController? textEditingController;
  final EdgeInsetsGeometry? margin;
  final int maxLines;
  final TextInputType? textInputType;
  final List<TextInputFormatter>? inputFormatters;
  final bool? isReadOnly;
  final void Function()? onTap;
  const CustomTextField(
      {Key? key,
      required this.hintText,
      this.margin,
      this.textEditingController,
      this.maxLines = 1,
      this.textInputType,
      this.onTap,
      this.inputFormatters,
      this.isReadOnly})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin ?? const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
          color: AppColors.background,
          borderRadius: BorderRadius.circular(22),
          boxShadow: const [
            BoxShadow(
                color: Color.fromRGBO(90, 108, 234, 0.07),
                offset: Offset(0, 0),
                spreadRadius: 10,
                blurRadius: 50)
          ]),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      alignment: Alignment.center,
      child: TextField(
        onTap: onTap,
        readOnly: isReadOnly ?? onTap != null,
        inputFormatters: inputFormatters,
        controller: textEditingController,
        maxLines: maxLines,
        keyboardType: textInputType,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: hintText,
            hintStyle: const TextStyle(color: Color.fromARGB(47, 38, 38, 38))),
      ),
    );
  }
}

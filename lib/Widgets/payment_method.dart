import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';

class PaymentMethodButton extends StatelessWidget {
  final String icon;
  final String accountNumber;
  final List<Color> gradientColors;
  final double? iconSize;
  final void Function()? onTap;
  const PaymentMethodButton(
      {Key? key,
      required this.icon,
      required this.accountNumber,
      required this.gradientColors,
      this.onTap,
      this.iconSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 73,
        margin: const EdgeInsets.symmetric(horizontal: 20),
        padding: const EdgeInsets.only(left: 19, right: 23),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          boxShadow: const [
            BoxShadow(
                color: Color.fromRGBO(90, 108, 234, 0.07000000029802322),
                offset: Offset(12, 26),
                blurRadius: 50)
          ],
          border: Border.all(
            color: const Color.fromRGBO(244, 244, 244, 1),
            width: 1,
          ),
          gradient: LinearGradient(
              end: const Alignment(0.97, 1.179),
              begin: const Alignment(-.75, 0.03),
              colors: gradientColors),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset(icon, height: iconSize ?? 17),
            Text(
              accountNumber,
              style: TextStyle(color: AppColors.background.withOpacity(.3)),
            )
          ],
        ),
      ),
    );
  }
}

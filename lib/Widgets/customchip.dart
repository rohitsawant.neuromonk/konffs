import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';

class CustomChip1 extends StatelessWidget {
  final String label;
  final String? discount;
  final Icon icon;
  final Color? chipColor;
  final Color? labelColor;
  final EdgeInsets? chipPadding;
  final EdgeInsets? chipMargin;

  const CustomChip1({
    super.key,
    required this.label,
    this.chipColor,
    this.labelColor,
    this.chipPadding,
    this.chipMargin,
    this.discount,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: chipMargin ?? const EdgeInsets.only( top: 10.5),
      padding: chipPadding ??
          const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
      decoration: BoxDecoration(
          color: chipColor ?? Color.fromRGBO(9, 115, 130, 1),
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          icon,
          const SizedBox(width: 5,),
          Text(
            label,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.w600,
                color: labelColor ?? AppColors.background),
          ),
        ],
      ),
    );
  }
}

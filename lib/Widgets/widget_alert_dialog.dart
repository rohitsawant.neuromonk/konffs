import 'package:flutter/material.dart';

import '../Models/models.dart';
import 'widget_text.dart';

class WidgetAlertDialog extends StatelessWidget {
  final String title;
  final Widget content;
  final Function positiveButton, no;
  const WidgetAlertDialog(
      {Key? key,
        required this.title,
        required this.content,
        required this.positiveButton,
        required this.no})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: content,
      actions: <Widget>[
        TextButton(
          child: const WidgetText(
            text: 'No',
            textType: EnumTextType.body,
          ),
          onPressed: () {
            // if (no != null) {
            //   no();
            // } else {
            Navigator.of(context).pop();
            // }
          },
        ),
        TextButton(
            child: Text(
              'Yes',
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              positiveButton();
            }),
      ],
    );
  }
}

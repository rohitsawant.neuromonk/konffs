import 'package:flutter/material.dart';

class PymentText extends StatelessWidget {
  const PymentText({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> str = [
      "Get More Customers",
    ];
    List<String> str1 = [
      "Get Genuine Reviews",
    ];
    List<String> str2 = [
      "Boost Your Business",
    ];
    List<String> str3 = [
      "Build Your Brand",
    ];
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 110, top: 50),
          child: Column(
            children: str.map((strone) {
              return Row(children: [
                Text(
                  "\u2022",
                  style: TextStyle(fontSize: 16),
                ), //bullet text
                SizedBox(
                  width: 10,
                ), //space between bullet and text
                Expanded(
                  child: Text(
                    strone,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ), //text
                )
              ]);
            }).toList(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 110, top: 10),
          child: Column(
            children: str1.map((strone) {
              return Row(children: [
                Text(
                  "\u2022",
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    strone,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                )
              ]);
            }).toList(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 110, top: 10),
          child: Column(
            children: str2.map((strone) {
              return Row(children: [
                Text(
                  "\u2022",
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    strone,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                )
              ]);
            }).toList(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 110, top: 10),
          child: Column(
            children: str3.map((strone) {
              return Row(children: [
                Text(
                  "\u2022",
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Text(
                    strone,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                )
              ]);
            }).toList(),
          ),
        ),
      ],
    );
  }
}

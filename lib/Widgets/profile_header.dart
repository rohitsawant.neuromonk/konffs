import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';

class ProfileHeader extends StatelessWidget {
  final String title;
  final void Function()? onBack;
  const ProfileHeader({Key? key, required this.title, this.onBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          onTap: onBack,
          child: Container(
            height: 35,
            width: 35,
            margin: const EdgeInsets.only(right: 80),
            decoration: BoxDecoration(
                color: AppColors.lightGrey,
                borderRadius: BorderRadius.circular(8)),
            alignment: Alignment.center,
            child: const Icon(
              Icons.arrow_back_ios_new,
              size: 20,
            ),
          ),
        ),
        Text(
          title,
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
        ),
      ],
    );
  }
}

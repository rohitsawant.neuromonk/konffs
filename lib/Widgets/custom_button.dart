import 'package:flutter/material.dart';

import '../Utils/app_colors.dart';

class CustomButton extends StatelessWidget {
  final String label;
  final Color? color;
  final void Function()? onTap;
  const CustomButton({Key? key, required this.label, this.onTap, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 57,
        margin: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          gradient: const LinearGradient(
              begin: Alignment(-0.9752321839332581, 1.1797842702776506e-9),
              end: Alignment(2.8603663881909824e-9, 0.03538257256150246),
              colors: [
                Color.fromRGBO(98, 226, 107, 1),
                Color.fromRGBO(45, 182, 88, 1)
              ]),
        ),
        alignment: Alignment.center,
        child: Text(
          label,
          style: const TextStyle(
              color: AppColors.background,
              fontSize: 16,
              fontWeight: FontWeight.w700),
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart';

import '../Utils/app_constants.dart';

class InfoController extends GetxController {
  final RxBool _isLoading = false.obs;

  RxBool get isLoading => _isLoading;

  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final mobileController = TextEditingController();
  final otpController = TextEditingController();
  String address = 'N/A';
  String coordinates = '';
  // PersonalInfo? personalInfo;

  String userName = "Sir";
  String? selectedCountryCode;

  // Future<bool> getPersonalInfo() async {
  //   _isLoading.value = true;
  //   try {
  //     String? uid = FirebaseAuth.instance.currentUser?.uid;

  //     if (uid == null) {
  //       throw "User doesn't exists";
  //     }

  //     final snapshot = await FirebaseFirestore.instance.doc("users/$uid").get();

  //     personalInfo = PersonalInfo.fromJson(snapshot.get("info") as Map);

  //     userName =
  //         "${personalInfo?.firstName ?? "Sir"} ${personalInfo?.lastName ?? ""}";
  //     address = personalInfo?.address ?? 'N/A';
  //     coordinates = personalInfo?.coordinates ?? '';
  //     debugPrint("Fetched Data --> $userName");
  //     _isLoading.value = false;
  //     return true;
  //   } catch (e) {
  //     _isLoading.value = false;
  //     debugPrint("Personal Info Error ---> $e");
  //     return false;
  //   }
  // }

  // Future<bool> savePersonalInfo([bool saveJustAddress = false]) async {
  //   _isLoading.value = true;
  //   try {
  //     String? uid = FirebaseAuth.instance.currentUser?.uid;

  //     if (uid == null) {
  //       throw "User doesn't exists";
  //     }

  //     final updatedInfo = PersonalInfo(
  //         firstName: firstNameController.text,
  //         lastName: lastNameController.text,
  //         mobileNumber:
  //             '${selectedCountryCode ?? ''} ${mobileController.text}');

  //     await FirebaseFirestore.instance.doc("users/$uid").set({
  //       "info": saveJustAddress
  //           ? {'address': address, 'coordinates': coordinates}
  //           : updatedInfo.toJson()
  //     }, SetOptions(merge: true));

  //     Get.snackbar("Hurray",
  //         "Successfully saved ${saveJustAddress ? 'location' : 'Personal info'}");
  //     // _isLoading.value = false;
  //     return true;
  //   } catch (e) {
  //     Get.snackbar("Oops!", "Something went wrong while saving personal info");
  //     debugPrint("Error Saving Business Info ----> $e");
  //     _isLoading.value = false;

  //     return false;
  //   }
  // }

  // Future<void> deleteAccount() async {
  //   try {
  //     String? uid = FirebaseAuth.instance.currentUser?.uid;
  //     if (uid == null) throw "User doesn't exists";

  //     await FirebaseAuth.instance.currentUser!.delete();
  //     await FirebaseAuth.instance.signOut();
  //     await GoogleSignIn().signOut();
  //     await FirebaseFirestore.instance.doc("users/$uid").delete();
  //   } catch (e) {
  //     Get.snackbar("Oops!", "Something went wrong while deleting account");
  //     log(e.toString(), name: 'Delete Business Error');
  //   }
  // }

  Future<String> getFullAddress(String coordinates) async {
    try {
      final uri = Uri.parse(
          'https://maps.google.com/maps/api/geocode/json?key=AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c-XOuSJqe6_c&language=en&latlng=$coordinates');
      final response = await get(uri);

      if (response.statusCode != 200) {
        return response.reasonPhrase ?? 'Api Error';
      }

      final jsonResponse = jsonDecode(response.body);
      address = jsonResponse['results'][0]['formatted_address'].toString();
      return address;
    } catch (error) {
      log(error.toString(), name: 'Full Address Error');
      return 'N/A';
    }
  }
}

enum EnumNoDataFoundType {
  internet,
  searchResult,
  other,
  followUp,
  adminConfirmation
}

enum EnumTextInputType { mobile, email, digitsWithDecimal, password, remark }

enum EnumTextField { dropdown, datePicker, timePicker }

enum EnumApiType { get, post, put, delete }

enum EnumTextType {
  display,
  bigTitle,
  title,
  subtitle,
  bigText,
  body,
  caption,
  small
}

enum EnumGender { male, female, other }

enum EnumLanguages { english, hindi, marathi }

enum EnumErrors { errNotFound, errServer, errServiceUnavailable, errNoInternet }

enum EnumBottomSheetType { alert, warning, confirmation, none }

enum EnumDateFormat {
  monthDateYear,
  dayMonthDateYear,
  dateYearMonth,
  yearDateMonth
}

enum EnumInfoType { neutral, success, alert, warning }

enum EnumEnvironment { staticData, local, server }

class DropdownItemData {
  String name;
  dynamic value;
  DropdownItemData({
    required this.name,
    required this.value,
  });
}

class ModelCustomers {
  String customerId, customerName, address;
  int totalClaims, totalBatteries;
  ModelCustomers({
    required this.customerId,
    required this.customerName,
    required this.address,
    required this.totalBatteries,
    required this.totalClaims,
  });
  static initialize() {
    return ModelCustomers(
      customerId: 'customerId',
      customerName: 'customerName',
      address: 'address',
      totalBatteries: 0,
      totalClaims: 0,
    );
  }
}

class ModelSerialNumber {
  String serialNumber,
      itemCode,
      invoiceNumber,
      invoiceDate,
      expiryDate,
      brandName,
      claimStatus,
      customerName,
      pickupPoint,
      claimNo,
      warrantyStatus;
  bool isVerified;
  ModelSerialNumber({
    required this.customerName,
    required this.pickupPoint,
    required this.claimNo,
    required this.serialNumber,
    required this.itemCode,
    required this.brandName,
    required this.claimStatus,
    required this.invoiceNumber,
    required this.invoiceDate,
    required this.expiryDate,
    required this.warrantyStatus,
    required this.isVerified,
  });
  static initialize() {
    return ModelSerialNumber(
      customerName: 'customerName',
      pickupPoint: 'pickupPoint',
      claimNo: 'claimNo',
      serialNumber: 'serialNumber',
      itemCode: 'itemCode',
      brandName: 'brandName',
      claimStatus: 'claimStatus',
      invoiceNumber: 'invoiceNumber',
      invoiceDate: DateTime.now().toString(),
      expiryDate: DateTime.now().toString(),
      warrantyStatus: 'warrantyStatus',
      isVerified: false,
    );
  }
}

class ModelClaims {
  String claimNumber;
  int total, pickedUp;
  ModelClaims({
    required this.claimNumber,
    required this.total,
    required this.pickedUp,
  });
}

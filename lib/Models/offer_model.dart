import 'dart:ffi';

import 'package:vendorapp/Models/offer.dart';

class Offers {
  int id, businessDetailsId, isActive;
  String offerDetails, categoryType;
  DateTime offerStartDate, offerEndDate, createdAt, updatedAt;
  String discount;
  Offers({
    required this.id,
    required this.offerDetails,
    required this.businessDetailsId,
    required this.categoryType,
    required this.createdAt,
    required this.discount,
    // required this.getFreeCount,
    required this.isActive,
    // required this.minBuyCount,
    required this.offerEndDate,
    required this.offerStartDate,
    // required this.termsAndConditions,
    required this.updatedAt,
  });
  static Offers fromMap(data) {
    return Offers(
        id: data['id']??0,
        offerDetails: data['offer_details']??'',
        businessDetailsId: int.parse("${data['business_details_id']}"),
        categoryType: data['category_type']??'',
        createdAt: DateTime.parse("${data['created_at']}"),
        discount: data['discount']??'',
        // getFreeCount: int.parse("${data['get_free_count']}"),
        isActive: int.parse("${data['is_active']}"),
        // minBuyCount: int.parse("${data['min_buy_count']}"),
          offerEndDate: data['offer_end_date'] != null
          ? DateTime.parse("${data['offer_end_date']}")
          : DateTime.now(),
        // offerEndDate: DateTime.parse("${data['offer_end_date']}"),
         offerStartDate: data['offer_start_date'] != null
          ? DateTime.parse("${data['offer_start_date']}")
          : DateTime.now(),
        // offerStartDate: data['offer_start_date'],
         updatedAt: DateTime.parse("${data['updated_at']}"));
        // termsAndConditions: data['terms_and_conditions']??'');
       
  }
   static List<Offers> listFromMap(List data) =>
      data.map((e) => Offers.fromMap(e)).toList();

      static Offers init()=>Offers(id: 0, offerDetails: '', businessDetailsId: 0, categoryType: '', createdAt: DateTime.now(), discount: '', isActive: 0, offerEndDate: DateTime.now(), offerStartDate: DateTime.now(), updatedAt: DateTime.now());
}

import 'package:flutter/material.dart';

class ModelProvider with ChangeNotifier {
  int currentIndex = 0, notificationCount = 0, currentPlaceOrderFormIndex = 0;
  bool showNotificationDot = false,
      noInternet = false,
      isLoggedIn = false,
      cameraOn = false,
      isMismatched = false;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  Locale lang = const Locale('en');
  ThemeMode themeMode = ThemeMode.light;
  String claimNumber = '', customerId = '', scannedBarcode = '';

  // App Language
  get getLanguage => lang;
  set setLanguage(Locale language) {
    lang = language;
    notifyListeners();
  }

  // App Theme
  get getThemeMode => themeMode;
  set setThemeMode(ThemeMode theme) {
    themeMode = theme;
    notifyListeners();
  }

  // Current Navigation Screen Index
  get getCurrentIndex => currentIndex;
  set setCurrentIndex(int index) {
    currentIndex = index;
    notifyListeners();
  }

  // Check User Logged In
  get getUserIsLoggedIn => isLoggedIn;
  set setUserLoggedIn(bool index) {
    isLoggedIn = index;
    notifyListeners();
  }

  // Notification Count
  get getNotificationCount => notificationCount;
  set updateNotificationCount(int index) {
    notificationCount = index;
    notifyListeners();
  }

  // Notification Dot
  get getNotificationDot => showNotificationDot;
  set setNotificationDot(bool isDotVisible) {
    showNotificationDot = isDotVisible;
    notifyListeners();
  }

  // Internet Connection
  get getInternetConnection => noInternet;
  set setInternetConnection(bool index) {
    noInternet = index;
    notifyListeners();
  }

  // Claim Number
  get getCustomerId => customerId;
  set setCustomerId(String id) {
    customerId = id;
    notifyListeners();
  }

  // Claim Number
  get getClaimNumber => claimNumber;
  set setClaimNumber(String number) {
    claimNumber = number;
    notifyListeners();
  }

  // Get Camera Status
  get getCameraOn => cameraOn;
  set setCameraOn(bool cameraStatus) {
    cameraOn = cameraStatus;
    notifyListeners();
  }

  // Get Mismatched
  get getMismatched => isMismatched;
  set setMismatched(bool flag) {
    isMismatched = flag;
    notifyListeners();
  }

  // Get Mismatched
  get getScannedBarcode => scannedBarcode;
  set setScannedBarcode(String barcode) {
    scannedBarcode = barcode;
    notifyListeners();
  }
}



import 'package:flutter/material.dart';

import '../Controllers/business_info_controller.dart';
import '../Models/business_search_model.dart';

class CustomSearchDelegate extends SearchDelegate<Results> {
  final String location;

  CustomSearchDelegate(this.location);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    query;
    return const Icon(Icons.search);
  }

  @override
  // TODO: implement searchFieldLabel
  String? get searchFieldLabel => 'Restaurant Name';
  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder(
      future: BusinessInfoController().getBusinessIds(query, location),
      builder: (context, snapshot) {
        final businessSearchModel = snapshot.data;
        return snapshot.connectionState == ConnectionState.done
            ? businessSearchModel?.results?.isEmpty == null
                ? const Center(
                    child: Text("No Results Found!"),
                  )
                : ListView.separated(
                    itemCount: businessSearchModel?.results?.length ?? 0,
                    itemBuilder: (context, index) {
                      final result = businessSearchModel!.results![index];
                      return ListTile(
                        title: Text(result.name ?? ""),
                        subtitle: Text(result.vicinity ?? ""),
                        onTap: () {
                          close(context, result);
                        },
                      );
                    },
                    separatorBuilder: (context, index) => const Divider(),
                  )
            : const Center(
                child: CircularProgressIndicator(),
              );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return const SizedBox();
  }
}

// ignore_for_file: constant_identifier_names

class RouteNames {
  static const String ONBOARDING_SCREEN = "onboarding_screen";
  static const String WELCOME_SCREEN = "welcome_screen";
  static const String SPLASH_SCREEN = "splash_screen";
  static const String BUSINESS_INFO_SCREEN = "business_info_screen";
  static const String CREATE_ACCOUNT_SCREEN = "create_account_screen";
  static const String PAYMENT_SCREEN = "payment_screen";
   static const String SUBSCRIPTION_SCREEN = "subscription_screen";
  static const String SUCCESS_SCREEN = "success_screen";
  static const String PERSONAL_DATA_SCREEN = "personal_data_screen";
  static const String BOTTOM_BAR_SCREEN = "bottom_bar_screen";
  static const String NOTIFICATION_SCREEN = "notification_screen";
  static const String EDIT_OFFER_SCREEN = "edit_offer_screen";
  static const String PRIVACY_POLICY_SCREEN = "privacy_policy_screen";
  static const String ABOUT_US_SCREEN = "about_us_screen";


  static const String VERIFICATION= "verification";
  
  static const String VERIFICATION_SECOND= "verification_second";
  // static const String USER_BUSINESS_INFO_SCREEN = "business_info_screen";
  // static const String USER_ONBOARDING_SCREEN="user_onboarding";
  // static const String USER_PAYMENT_SCREEN = "payment_screen";
  // static const String USER_SUCCESS_SCREEN = "success_screen";
  // static const String USER_PERSONAL_DATA_SCREEN = "personal_data_screen";
  // static const String USER_BOTTOM_BAR_SCREEN = "bottom_bar_screen";
  // static const String USER_NOTIFICATION_SCREEN = "notification_screen";
  // static const String USER_EDIT_OFFER_SCREEN = "edit_offer_screen";
  // static const String USER_PRIVACY_POLICY_SCREEN = "privacy_policy_screen";
  // static const String USER_ABOUT_US_SCREEN = "about_us_screen";
}

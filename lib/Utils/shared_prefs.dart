import 'dart:developer';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static late SharedPreferences prefs;

  static Future initialize() async {
    try {
      prefs = await SharedPreferences.getInstance();
    } catch (e) {
      log(e.toString(), name: 'Initialize Shared Preferences');
    }
  }
}

class SharedPrefsKeys {
  static const String userStatus = 'user_status';
}


import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../Utils/app_colors.dart';
import '../../Widgets/profile_header.dart';

class DashboardScreen extends StatefulWidget {
    final void Function()? onBack;
  const DashboardScreen({super.key, this.onBack});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.background1,
        body: SafeArea(
            child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 30,
              top: 30,
            ),
            child:
          
             ProfileHeader(
              title: "Dashboard",
              onBack: () => Get.back(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, top: 60),
            child: Row(
              children: [
                Container(
                  height: 179,
                  width: 165,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: new DecorationImage(
                      image: new AssetImage(
                        "assets/images/binoculars 1.png",
                      ),
                      // colorFilter: ColorFilter.linearToSrgbGamma()
                      // fit: BoxFit.fitHeight,
                      scale: 4,
                    ),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 32,
                          offset: Offset.zero),
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '25',
                        style: TextStyle(
                            fontSize: 48, fontWeight: FontWeight.w800),
                      ),
                      Text(
                        'Offers Viewed',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 179,
                  width: 165,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: new DecorationImage(
                      image: new AssetImage(
                        "assets/images/shopping 1.png",
                      ),
                      // colorFilter: ColorFilter.linearToSrgbGamma()
                      // fit: BoxFit.fitHeight,
                      scale: 4,
                    ),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 32,
                          offset: Offset.zero),
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '7',
                        style: TextStyle(
                            fontSize: 48, fontWeight: FontWeight.w800),
                      ),
                      Text(
                        'Offers Claimed',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, top: 20),
            child: Row(
              children: [
                Container(
                  height: 179,
                  width: 165,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: new DecorationImage(
                      image: new AssetImage("assets/images/gold-star 1.png"),
                      scale: 4,
                    ),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 32,
                          offset: Offset.zero),
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '0',
                        style: TextStyle(
                            fontSize: 48, fontWeight: FontWeight.w800),
                      ),
                      Text(
                        'Reviews gained',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Container(
                  height: 179,
                  width: 165,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: new DecorationImage(
                      image: new AssetImage(
                        "assets/images/wishlist 1.png",
                      ),
                      scale: 4,
                    ),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 32,
                          offset: Offset.zero),
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '25',
                        style: TextStyle(
                            fontSize: 48, fontWeight: FontWeight.w800),
                      ),
                      Text(
                        'Offers Saved',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, top: 20),
            child: Row(
              children: [
                Container(
                  height: 179,
                  width: 165,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: new DecorationImage(
                      image: new AssetImage(
                        "assets/images/networking 1.png",
                      ),
                      scale: 4,
                    ),
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          blurRadius: 32,
                          offset: Offset.zero),
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '25',
                        style: TextStyle(
                            fontSize: 48, fontWeight: FontWeight.w800),
                      ),
                      Text(
                        'Offers Shared',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
        ])));
  }
}

import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/custom_back_button.dart';
import '../../Widgets/custom_button.dart';
import '../../Widgets/custom_text_field.dart';
import '../../helper/iconstant.dart';

class Verification extends StatefulWidget {
  // final String data;

  const Verification({
    Key? key,
  }) : super(key: key);

  @override
  State<Verification> createState() => _VerificationState();
}

class _VerificationState extends State<Verification> {
  final otpController = TextEditingController();
  bool isLoading = false;
  String userName = "", email = "", mobileNumber = "", contryCode = "";
  getInstance() async {
    await SharedPreferences.getInstance().then((value) {
      setState(() {
        // userName = value.getString(IConstant.userName) as String;
        // email = value.getString(IConstant.userEmailId) as String;
        mobileNumber = value.getString(IConstant.userMobileNo) as String;
        contryCode = value.getString(IConstant.userContryCode) as String;
        // mobile = value.getString(IConstant.userMobileNo) as String;
        isLoading = false;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getInstance();
  }

  FirebaseAuth auth = FirebaseAuth.instance;
  // void verifyUserPhoneNumber() {
  //   log("Mobile Number :{+${contryCode}${mobileNumber}}");
  //   auth.verifyPhoneNumber(
  //     phoneNumber: "+${contryCode}${mobileNumber}",
  //     verificationFailed: (FirebaseAuthException e) {
  //       print(e.message);
  //     },
  //     codeSent: (String verificationId, int? resendToken) {},
  //     codeAutoRetrievalTimeout: (String verificationId) {
  //       print('TimeOut');
  //     },
  //     verificationCompleted: (PhoneAuthCredential phoneAuthCredential) {},
  //   );
  // }

  String _verificationId = '';

  void _verifyPhoneNumber() async {
    log( "${contryCode}${mobileNumber}");
    await auth.verifyPhoneNumber(
      phoneNumber: "${contryCode}${mobileNumber}",
      
      verificationCompleted: (PhoneAuthCredential credential) async {
        await auth.signInWithCredential(credential);
        Get.snackbar('Otp', 'Send Successfully...!');
      },
      verificationFailed: (FirebaseAuthException e) {},
      codeSent: (String verificationId, int? resendToken) {
        setState(() {
          _verificationId = verificationId;
        });
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        setState(() {
          _verificationId = verificationId;

          Get.snackbar("Oops!", "Otp Exprired");
        });
      },
    );
  }

  void _signInWithOTP() async {
    if (otpController.text.isEmpty) {
      Get.snackbar("Oops!", "Please Enter your valide otp");
      return;
    }
    if (otpController.text.length != 6) {
      Get.snackbar("Oops!", "Enter your 6 digit otp");
      return;
    }
    try {
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: otpController.text,
      );

      UserCredential userCredential =
          await auth.signInWithCredential(credential);

      if (userCredential.user != null) {
 Get.toNamed(RouteNames.SUBSCRIPTION_SCREEN);
      } else {
        Get.snackbar("Oops!", "Sign-in failed. Please try again.");
      }
    } catch (e) {
      print("Sign-in error: $e");
      Get.snackbar("Oops!", "Sign-in failed. Please try again.");
    }
  }



  bool isSuccess = false;

  @override
  Widget build(BuildContext context) {
    String mobileNumber1 = "${mobileNumber}"; // Replace with your mobile number
 String last4Digits = mobileNumber1.substring(mobileNumber1.length - 4);
    return Scaffold(
      floatingActionButton: Container(
          padding: const EdgeInsets.only(bottom: 22, right: 10),
          child: InkWell(
              onTap: _signInWithOTP,
              child: Image.asset(AppImages.halfProgress, height: 60))),

      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomBackButton(),
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 20),
                child: Text(
                  "Verify Yourself$contryCode",
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppConstants.poppinsFonts),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 20, left: 25, right: 125),
                child: Text(
                  'Select which contact details should we use to verify',
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              const SizedBox(height: 50),
              InkWell(
                onTap: () => _verifyPhoneNumber(),
                child: Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Container(
                
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/images/Vector.png',
                          height: 50,
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            
                            Text(
                              'Via sms:',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 16),
                            ),
                            Text(
                             "* * * * * * " + last4Digits,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 55, left: 25, right: 35),
                child: Text(
                  'Note: If you do not hold any of this contact details then\nplease update it on google and come back later.',
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Center(
                child: SizedBox(
                  width: 300,
                  height: 80,
                  child: CustomButton(
                    label: 'Send OTP',
                    onTap: () async {
                      _verifyPhoneNumber();
                      // log('Phone otp  send');
                    },
                  ),
                ),
              ),
              Center(
                child: SizedBox(
                  width: 250,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Center(
                      child: CustomTextField(
                          hintText: "Enter OTP",
                          textEditingController: otpController,
                          textInputType: TextInputType.phone),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

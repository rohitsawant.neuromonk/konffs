import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:map_location_picker/map_location_picker.dart';


class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String address = "null";
  String autocompletePlace = "null";
  Prediction? initialValue;
  bool isLoading=false;
  Future<void> _setLocation() async {
    isLoading = true;
    await Get.to(() => 
    
    MapLocationPicker(
          backButton: IconButton(
              onPressed: () => isLoading = false,
              icon: const Icon(Icons.arrow_back)),
          apiKey: 'AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c',
          onSuggestionSelected: (selectedPlace) => _pickLocation(
              selectedPlace?.result.formattedAddress,
              '${selectedPlace?.result.geometry?.location.lat},${selectedPlace?.result.geometry?.location.lng}'),
          onNext: (GeocodingResult? result) => _pickLocation(
              result?.formattedAddress,
              '${result?.geometry.location.lat},${result?.geometry.location.lng}'),
        ));
    isLoading = false;
  }

  void _pickLocation(String? formattedAddress, String coordinates) async {
    if (formattedAddress == null) {
      isLoading = false;
      return;
    }
    log(formattedAddress, name: 'Picked Address');
    // infoController.address = formattedAddress;
    // infoController.coordinates = coordinates;
    // await infoController.savePersonalInfo(true);
    // SharedPrefs.prefs.setString(
    //     SharedPrefsKeys.userStatus, AppConstants.readyToUseAppStatus);
    // Get.offAllNamed(RouteNames.SUCCESS_SCREEN);
  }
  final TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('location picker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          PlacesAutocomplete(
            searchController: _controller,
            apiKey: "AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c",
            mounted: mounted,
            hideBackButton: false,
            onGetDetailsByPlaceId: (PlacesDetailsResponse? result) {
              if (result != null) {
                setState(() {
                  autocompletePlace = result.result.formattedAddress ?? "";
                });
              }
            },
          ),
          OutlinedButton(
            child: Text('show dialog'.toUpperCase()),
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: const Text('Example'),
                    content: PlacesAutocomplete(
                      apiKey: "AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c",
                      searchHintText: "Search for a place",
                      mounted: mounted,
                      hideBackButton: false,
                      initialValue: initialValue,
                      onSuggestionSelected: (value) {
                        setState(() {
                          autocompletePlace =
                              value.structuredFormatting?.mainText ?? "";
                          initialValue = value;
                        });
                      },
                      onGetDetailsByPlaceId: (value) {
                        setState(() {
                          address = value?.result.formattedAddress ?? "";
                        });
                      },
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          const Spacer(),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              "Google Map Location Picker\nMade By Arvind 😃 with Flutter 🚀",
              textAlign: TextAlign.center,
              textScaleFactor: 1.2,
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
          TextButton(
            onPressed: () => Clipboard.setData(
              const ClipboardData(text: "https://www.mohesu.com"),
            ).then(
              (value) => ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text("Copied to Clipboard"),
                ),
              ),
            ),
            child: const Text("https://www.mohesu.com"),
          ),
          const Spacer(),
          Center(
            child: ElevatedButton(
              child: const Text('Pick location'),
              onPressed: () async {
                _setLocation();
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) {
                //       return 
                //       MapLocationPicker(
                //         apiKey: "AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c",
                //         popOnNextButtonTaped: true,
                //         // currentLatLng: const LatLng(29.146727, 76.464895),
                //          currentLatLng: const LatLng(18.482883, 73.868788),
                //         onNext: (GeocodingResult? result) {
                //           if (result != null) {
                //             setState(() {
                //               address = result.formattedAddress ?? "";
                //             });
                //           }
                //         },
                //         onSuggestionSelected: (PlacesDetailsResponse? result) {
                //           if (result != null) {
                //             setState(() {
                //               autocompletePlace =
                //                   result.result.formattedAddress ?? "";
                //             });
                //           }
                //         },
                //       );
                //     },
                //    ),
                //    );
                
              },
            ),
          ),
          const Spacer(),
          ListTile(
            title: Text("Geocoded Address: $address"),
          ),
          ListTile(
            title: Text("Autocomplete Address: $autocompletePlace"),
          ),
          const Spacer(
            flex: 3,
          ),
        ],
      ),
    );
  }
}
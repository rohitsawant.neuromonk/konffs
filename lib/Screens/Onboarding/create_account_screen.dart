import 'dart:developer';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:map_location_picker/map_location_picker.dart';
import '../../Controllers/business_info_controller.dart';
import '../../Models/business_search_model.dart';
import '../../Models/sign_up.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/custom_search_delegate.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/custom_back_button.dart';
import '../../Widgets/custom_text_field.dart';
import '../../helper/helper_function.dart';
import '../../services/service_signup.dart';

class CreateAccountScreen extends StatefulWidget {
  final String data;

  const CreateAccountScreen({Key? key, required this.data}) : super(key: key);

  @override
  State<CreateAccountScreen> createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  final _businessInfoController = Get.put(BusinessInfoController());
  final TextEditingController emailController = TextEditingController();
  bool isSuccess = false;
  final RxString _businessName = "".obs;
  String? position;
  SignUp signUp = SignUp.init();
  void _saveDetails() async {
    if (_businessInfoController.firstNameController.text.isEmpty ||
        _businessInfoController.lastNameController.text.isEmpty ||
        _businessInfoController.googleBusinessId.text.isEmpty ||
        _businessInfoController.mobileController.text.isEmpty) {
      Get.snackbar("Oops!", "Any of the fields cannot be left empty");
      return;
    }
    if (_businessInfoController.mobileController.text.length != 10) {
      Get.snackbar("Oops!", "Enter valid mobile number");
      return;
    }

    setState(() {
      signUp.mobileNo = _businessInfoController.mobileController.text;
      signUp.fname = _businessInfoController.firstNameController.text;
      signUp.email = widget.data;
      signUp.lname = _businessInfoController.lastNameController.text;
      signUp.countryCode =
          "${_businessInfoController.selectedCountryCode}";
// log("selectedCountryCode${_businessInfoController.selectedCountryCode}");
      signUp.companyname = _businessInfoController.businessName;
      signUp.contactNumber = "9172268905";
      signUp.ratings = _businessInfoController.rating;
      signUp.placeId = _businessInfoController.googleBusinessId.text;
      List<String> coordinateList =
          _businessInfoController.businessCoordinates.split(',');
      String latitude = coordinateList[0];
      String longitude = coordinateList[1];
      signUp.latitude = "${latitude}";
      signUp.longitude = "${longitude}";
      String data = _businessInfoController.googleBusinessname.text;
      log("${data}");
      signUp.addressLine1 = 'Pune';
      signUp.addressLine2 = 'Pune';
      signUp.city = 'pune';
      signUp.state = 'MH';
      signUp.pincode = '410236';
      _businessInfoController.businessName =
          _businessInfoController.businessName;
    });

    try {
      Map<String, dynamic> data = await ServiceSignUp.signup(context, signUp);

      if (data.isNotEmpty) {
        HelperFunction.assignDataToPrefs(data['data'] ?? {}, isLogin: true);
        // var register = data['data'];
        log("${data}");
        await Future.delayed(Duration(seconds: 2));
        Get.toNamed(RouteNames.VERIFICATION);
        // Navigator.push(context,MaterialPageRoute(builder: (context)=>const Verification()));
      }

      setState(() {
        isSuccess = false;
      });
    } catch (e) {
      setState(() {
        isSuccess = false;
      });
    }
  }

  Future<Position?> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    try {
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        Get.snackbar('Oops!',
            'Location services are disabled. Please enable it and grant permission from settings');
        return null;
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          Get.snackbar('Oops!',
              'Location permissions are denied. Grant permission from settings');
          return null;
        }
      }

      if (permission == LocationPermission.deniedForever) {
        Get.snackbar('Oops!',
            'Location permissions are permanently denied, we cannot request permissions. Grant permission from settings');
        return null;
      }

      return await Geolocator.getCurrentPosition();
    } catch (error) {
      Get.snackbar('Error!',
          'Something went wrong! Make sure you have granted us the location permission');
      return null;
    }
  }

  bool isLoading = false;
  Future<void> _setLocation() async {
    isLoading = true;
    await Get.to(() => MapLocationPicker(
          backButton: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.arrow_back)),
          placesBaseUrl: '',
          apiKey: AppConstants.googleMapsKey,
          onSuggestionSelected: (selectedPlace) => _pickLocation(
              selectedPlace?.result.formattedAddress,
              '${selectedPlace?.result.geometry?.location.lat},${selectedPlace?.result.geometry?.location.lng}'),
          onNext: (GeocodingResult? result) => _pickLocation(
              result?.formattedAddress,
              '${result?.geometry.location.lat},${result?.geometry.location.lng}'),
        ));
    isLoading = false;
  }

  void _pickLocation(String? formattedAddress, String coordinates) async {
    if (formattedAddress == null) {
      isLoading = false;
      return;
    }
    log(formattedAddress, name: 'Picked Address');
    position = '${coordinates}';

    Navigator.pop(context);
    // SharedPrefs.prefs.setString(
    //     SharedPrefsKeys.userStatus, AppConstants.readyToUseAppStatus);
    // _getGoogleBusinessId();
    // Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) => const CreateAccountScreen(data: '')));
  }

  void _getGoogleBusinessId() async {
    Get.dialog(
        WillPopScope(
          onWillPop: () async => false,
          child: const AlertDialog(
            content: LinearProgressIndicator(),
            title: Text('Loading Business...'),
          ),
        ),
        barrierDismissible: false);
    if (position == null) {
      await _setLocation().then((pos) {
        // if (pos != null)
        //position = '${pos.latitude},${pos.longitude}';
      });

      if (position == null) {
        isSuccess = false;
        return;
      }
    }

    if (mounted) {
      final result = await showSearch<Results>(
          context: context, delegate: CustomSearchDelegate(position!));

      bool isBusinessIdExists =
          (_businessInfoController.businessInfo?.googleBusinessId ==
                  (result?.placeId ?? ""))
              ? false
              : await _businessInfoController
                  .checkIfBusinessIdExists(result?.placeId ?? "");

      Get.back();
      if (isBusinessIdExists) {
        Get.snackbar('Oops!', 'Business with this place id already exists');
        return;
      }
      if (result?.name != null) {
        _businessName.value = result!.name!;
        _businessInfoController.businessName = _businessName.value;
        _businessInfoController.businessCoordinates = _businessName.value;
      }
      _businessInfoController.rating = (result?.rating ?? 0.0).toString();
      _businessInfoController.types = result?.types ?? [];
      _businessInfoController.businessCoordinates =
          '${result?.geometry?.location?.lat ?? ''},${result?.geometry?.location?.lng ?? ''}';
      _businessInfoController.googleBusinessId.text = result?.placeId ?? "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
          padding: const EdgeInsets.only(bottom: 22, right: 10),
          child: InkWell(
              onTap: _saveDetails,
              child: Image.asset(AppImages.halfProgress, height: 60))),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomBackButton(),
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 20),
                child: Text(
                  "${isSuccess ? "Edit" : "Fill"} this details",
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppConstants.poppinsFonts),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 55, left: 25, right: 125),
                child: Text(
                  AppConstants.createAccountDes,
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              const SizedBox(height: 50),
              CustomTextField(
                  hintText: "First Name",
                  textEditingController:
                      _businessInfoController.firstNameController),
              const SizedBox(height: 30),
              CustomTextField(
                  hintText: "last Name",
                  textEditingController:
                      _businessInfoController.lastNameController),
              const SizedBox(height: 30),
              Row(
                children: [
                  CountryCodePicker(
                      initialSelection:
                          _businessInfoController.selectedCountryCode,
                      onChanged: (value) => _businessInfoController
                          .selectedCountryCode = value.dialCode,
                      padding: const EdgeInsets.only(left: 15, right: 5)),
                  Expanded(
                    child: CustomTextField(
                        margin: const EdgeInsets.only(right: 15),
                        hintText: "Mobile Number",
                        textEditingController:
                            _businessInfoController.mobileController,
                        textInputType: TextInputType.phone),
                  ),
                ],
              ),
              const SizedBox(height: 30),
              CustomTextField(
                hintText: "Google Business Id",
                isReadOnly: true,
                textEditingController: _businessInfoController.googleBusinessId,
                onTap: _getGoogleBusinessId,
              ),
              //  CustomTextField(
              //   hintText: "Google Business Id",
              //   isReadOnly: true,
              //   textEditingController: _businessInfoController.googleBusinessId,
              //   onTap: _getGoogleBusinessId,
              // ),
              Padding(
                padding: const EdgeInsets.only(left: 30, top: 15),
                child: Obx(() => _businessName.value.isEmpty
                    ? const SizedBox()
                    : Text("Business Name: $_businessName")),
              ),
              // TextButton(
              //     onPressed: () {
              //       _addBusiness();
              //     },
              //     child: Text('data'))
            ],
          ),
        ),
      ),
    );
  }
}

// import 'package:easy_loading_button/easy_loading_button.dart';
import 'package:easy_loading_button/easy_loading_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Utils/shared_prefs.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({
    super.key,
  });

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  final PageController pageController = PageController();
  void _onSubmit() {
    setState(() => isLoading = true);
    Future.delayed(const Duration(seconds: 3), () {
      if (_currentPage == _carouselImages.length - 1) {
        SharedPrefs.prefs.setString(
            SharedPrefsKeys.userStatus, AppConstants.loginLeftStatus);
        Get.toNamed(RouteNames.WELCOME_SCREEN);
      }
      setState(() {
        isLoading = false;
      });
    });
    //       },
  }

  int _currentPage = 0;

  final _carouselImages = [
    AppImages.growBusinessIllustration1,
  ];
  final _carouselImages1 = [
    AppImages.offerCreateIllustration1,
  ];
  final _carouselImages2 = [
    AppImages.verifyIllustration1,
  ];

  onButtonPressed() async {
    await Future.delayed(const Duration(seconds: 3), () => 42);
    return () {
      if (_currentPage == _carouselImages.length - 1) {
        SharedPrefs.prefs.setString(
            SharedPrefsKeys.userStatus, AppConstants.loginLeftStatus);
        Get.toNamed(RouteNames.WELCOME_SCREEN);
      }
      pageController.nextPage(
          duration: const Duration(milliseconds: 800),
          curve: Curves.fastOutSlowIn);
    };
  }

  final _introKey = GlobalKey<IntroductionScreenState>();
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      key: _introKey,
      pages: [
        PageViewModel(
          title: '',
          bodyWidget: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 380,
                  height: 600,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.2),
                      style: BorderStyle.solid,
                      width: 2.0,
                    ),
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 570,
                        width: 400,
                        child: PageView(
                            onPageChanged: (value) => _currentPage = value,
                            controller: pageController,
                            children: _carouselImages
                                .map<Widget>((img) => Padding(
                                      padding: const EdgeInsets.all(14),
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 20),
                                            child: Column(
                                              children: [
                                                Image.asset(img),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 50),
                                                  child: Text(
                                                    AppConstants.firsttext,
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                .toList()),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        PageViewModel(
          title: '',
          bodyWidget: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 380,
                  height: 600,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.2),
                      style: BorderStyle.solid,
                      width: 2.0,
                    ),
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 570,
                        width: 400,
                        child: PageView(
                            onPageChanged: (value) => _currentPage = value,
                            controller: pageController,
                            children: _carouselImages1
                                .map<Widget>((img) => Padding(
                                      padding: const EdgeInsets.all(14),
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 20),
                                            child: Column(
                                              children: [
                                                Image.asset(img),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 50),
                                                  child: Text(
                                                    AppConstants.secondtext,
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                .toList()),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        PageViewModel(
          title: '',
          bodyWidget: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 380,
                  height: 600,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.withOpacity(0.2),
                      style: BorderStyle.solid,
                      width: 2.0,
                    ),
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 570,
                        width: 400,
                        child: PageView(
                            onPageChanged: (value) => _currentPage = value,
                            controller: pageController,
                            children: _carouselImages2
                                .map<Widget>((img) => Padding(
                                      padding: const EdgeInsets.all(14),
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 20),
                                            child: Column(
                                              children: [
                                                Image.asset(img),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 10),
                                                  child: Text(
                                                    AppConstants.secondtext,
                                                    style:
                                                        TextStyle(fontSize: 16),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 40,
                                                ),
                                                EasyButton(
                                                  idleStateWidget: const Text(
                                                    'Next',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                  loadingStateWidget:
                                                      const CircularProgressIndicator(
                                                    strokeWidth: 2.0,
                                                    valueColor:
                                                        AlwaysStoppedAnimation<
                                                            Color>(
                                                      Colors.white,
                                                    ),
                                                  ),
                                                  useEqualLoadingStateWidgetDimension:
                                                      true,
                                                  useWidthAnimation: false,
                                                  width: 250.0,
                                                  height: 50.0,
                                                  borderRadius: 20.0,
                                                  elevation: 2.0,
                                                  contentGap: 6.0,
                                                  buttonColor: Color.fromARGB(
                                                      255, 110, 218, 114),
                                                  onPressed: onButtonPressed,
                                                ),
                                                // CustomButton(
                                                //   label: 'Next',
                                                //   onTap: () {
                                                // if (_currentPage ==
                                                //     _carouselImages.length -
                                                //         1) {
                                                //   SharedPrefs.prefs.setString(
                                                //       SharedPrefsKeys
                                                //           .userStatus,
                                                //       AppConstants
                                                //           .loginLeftStatus);
                                                //   Get.toNamed(RouteNames
                                                //       .WELCOME_SCREEN);
                                                //     }
                                                //     pageController.nextPage(
                                                //         duration:
                                                //             const Duration(
                                                //                 milliseconds:
                                                //                     800),
                                                //         curve: Curves
                                                //             .fastOutSlowIn);
                                                //   },
                                                // ),

                                                // SizedBox(
                                                //   width: 200,
                                                //   child: Container(

                                                //       // padding:
                                                //       //     const EdgeInsets.only(
                                                //       //         left: 10,
                                                //       //         right: 10),
                                                //       // width:
                                                //       //     MediaQuery.of(context)
                                                //       //         .size
                                                //       //         .width,
                                                //       height: 50,

                                                //       // elevated button created and given style
                                                //       // and decoration properties
                                                //       child: ElevatedButton(
                                                //         style: ElevatedButton
                                                //             .styleFrom(
                                                //                 primary:
                                                //                     Colors.green),
                                                //         onPressed: () {
                                                //           setState(() {
                                                //             isLoading = true;
                                                //           });

                                                //           // we had used future delayed to stop loading after
                                                //           // 3 seconds and show text "submit" on the screen.
                                                //           Future.delayed(
                                                //               const Duration(
                                                //                   seconds: 3),
                                                //               () {
                                                //             setState(() {
                                                //               isLoading = false;
                                                //             });
                                                //           });
                                                //         },
                                                //         child: isLoading
                                                //             ? Row(
                                                //                 mainAxisAlignment:
                                                //                     MainAxisAlignment
                                                //                         .center,

                                                //                 // as elevated button gets clicked we will see text"Loading..."
                                                //                 // on the screen with circular progress indicator white in color.
                                                //                 //as loading gets stopped "Submit" will be displayed
                                                //                 children: const [
                                                //                   Text(
                                                //                     '',
                                                //                     style: TextStyle(
                                                //                         fontSize:
                                                //                             20),
                                                //                   ),
                                                //                   SizedBox(
                                                //                     width: 10,
                                                //                   ),
                                                //                   CircularProgressIndicator(
                                                //                     color: Colors
                                                //                         .white,
                                                //                   ),
                                                //                 ],
                                                //               )
                                                //             : const Text(
                                                //                 'Submit',style: TextStyle(fontSize: 20),),
                                                //       )
                                                //       ),
                                                // )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                .toList()),
                      ),
                    ],
                  ),
                ),

                // Padding(
                //   padding: const EdgeInsets.only(bottom: 0),
                //   child: CustomButton(
                //     label: 'Next',
                //     onTap: () {
                //       if (_currentPage == _carouselImages.length - 1) {
                //         SharedPrefs.prefs.setString(SharedPrefsKeys.userStatus,
                //             AppConstants.loginLeftStatus);
                //         Get.toNamed(RouteNames.BOTTOM_BAR_SCREEN);
                //       }
                //       pageController.nextPage(
                //           duration: const Duration(milliseconds: 800),
                //           curve: Curves.fastOutSlowIn);
                //     },
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ],
      showNextButton: false,
      showDoneButton: false,
    );
  }
}

import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:map_location_picker/map_location_picker.dart';
import '../../Controllers/business_info_controller.dart';
import '../../Models/business_search_model.dart';
import '../../Models/sign_up.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/custom_search_delegate.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/custom_back_button.dart';
import '../../Widgets/custom_text_field.dart';
import '../../helper/helper_function.dart';
import '../../services/service_signup.dart';

class CreateBusiness extends StatefulWidget {
  final String data;

  const CreateBusiness({Key? key, required this.data}) : super(key: key);

  @override
  State<CreateBusiness> createState() => _CreateBusinessState();
}

class _CreateBusinessState extends State<CreateBusiness> {
  final _businessInfoController = Get.put(BusinessInfoController());
  final TextEditingController emailController = TextEditingController();
  bool isOldUser = false;
  bool isSuccess = false;
  bool isSavingInfo = false;

  final RxString _businessName = "".obs;

  String? position;

  @override
  void initState() {
    super.initState();
// location();
    _fetchData();
    // checkLogin();
  }

  _fetchData() async {
    isOldUser = await _businessInfoController.getBusinessInfo();

    if (isOldUser) {
      _businessInfoController.firstNameController.text =
          _businessInfoController.businessInfo?.firstName ?? "";
      _businessInfoController.lastNameController.text =
          _businessInfoController.businessInfo?.lastName ?? "";
      _businessInfoController.googleBusinessId.text =
          _businessInfoController.businessInfo?.googleBusinessId ?? "";
      position =
          _businessInfoController.businessInfo?.googleBusinessCoordinates;

      List<String>? mobileNo =
          (_businessInfoController.businessInfo?.mobileNumber)?.split(' ');

      if (mobileNo?.length == 2) {
        _businessInfoController.selectedCountryCode = mobileNo?[0];
        _businessInfoController.mobileController.text = mobileNo?[1] ?? '';
      } else {
        _businessInfoController.mobileController.text = mobileNo?[0] ?? '';
      }

      setState(() {});
    } else {
      _determinePosition().then((pos) {
        if (pos != null) position = '${pos.latitude},${pos.longitude}';
      });
    }
  }

  SignUp signUp = SignUp.init();
  bool loading = false;

  void _saveDetails() async {
    if (_businessInfoController.firstNameController.text.isEmpty ||
        _businessInfoController.lastNameController.text.isEmpty ||
        // _businessInfoController.googleBusinessId.text.isEmpty ||
        _businessInfoController.mobileController.text.isEmpty) {
      Get.snackbar("Oops!", "Any of the fields cannot be left empty");
      return;
    }
    if (_businessInfoController.mobileController.text.length != 10) {
      Get.snackbar("Oops!", "Enter valid mobile number");
      return;
    }

    setState(() {
      signUp.mobileNo = _businessInfoController.mobileController.text;
      signUp.fname = _businessInfoController.firstNameController.text;
      signUp.email = widget.data;
      signUp.lname = _businessInfoController.lastNameController.text;
      // signUp.countryCode =
      //     int.parse("${_businessInfoController.selectedCountryCode}");
    });

    try {
      Map<String, dynamic> data = await ServiceSignUp.signup(context, signUp);

      if (data.isNotEmpty) {
        HelperFunction.assignDataToPrefs(data['data'] ?? {}, isLogin: true);
        var register = data['business_details'];
        log("register api: :${data}");
        if (register != null) {
          Get.toNamed(RouteNames.CREATE_ACCOUNT_SCREEN);
        }

        log("register api: :${register}");
        // _addBusiness();
        // Get.toNamed(RouteNames.VERIFICATION);
      }

      setState(() {
        loading = false;
      });
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  AddBusiness addBusiness = AddBusiness.init();
  // bool loading = false;

  void _addBusiness() async {
    if (_businessInfoController.googleBusinessId.text.isEmpty) {
      Get.snackbar("Oops!", "Business Id are missing ..!");
      return;
    }

    setState(() {
      addBusiness.companyname = _businessInfoController.businessName;
      addBusiness.ratings = _businessInfoController.rating;
      addBusiness.placeId = _businessInfoController.googleBusinessId.text;

      List<String> coordinateList =
          _businessInfoController.businessCoordinates.split(',');
      String latitude = coordinateList[0];
      String longitude = coordinateList[1];
      addBusiness.latitude = "${latitude}";
      addBusiness.longitude = "${longitude}";
      addBusiness.addressLine1 = 'Pune';
      // log("${_businessInfoController.businessAddress}");
      addBusiness.addressLine2 = 'Pune';
      addBusiness.city = 'pune';
      addBusiness.state = 'MH';
      addBusiness.pincode = '410236';
    });

    try {
      Map<String, dynamic> data =
          await ServiceAddBusiness.addBusiness(context, addBusiness);
      if (data.isNotEmpty) {
        var addBusiness = data['messege'];
        // HelperFunction.assignDataToPrefs(data['data'] ?? {}, isLogin: true);
        // checkLogin();
        // Get.toNamed(RouteNames.VERIFICATION);
        log("add business :${addBusiness}");
      }

      setState(() {
        loading = false;
      });
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  Future<Position?> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    try {
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        Get.snackbar('Oops!',
            'Location services are disabled. Please enable it and grant permission from settings');
        return null;
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          Get.snackbar('Oops!',
              'Location permissions are denied. Grant permission from settings');
          return null;
        }
      }

      if (permission == LocationPermission.deniedForever) {
        Get.snackbar('Oops!',
            'Location permissions are permanently denied, we cannot request permissions. Grant permission from settings');
        return null;
      }

      return await Geolocator.getCurrentPosition();
    } catch (error) {
      Get.snackbar('Error!',
          'Something went wrong! Make sure you have granted us the location permission');
      return null;
    }
  }

  // String address = "null";
  // String autocompletePlace = "null";
  // Prediction? initialValue;
  bool isLoading = false;
  // Future<void> _setLocation() async {
  //   isLoading = true;
  //   await Get.to(() => MapLocationPicker(
  //         backButton:
  //             IconButton(onPressed: () {}, icon: const Icon(Icons.arrow_back)),
  //         placesBaseUrl: '',
  //         apiKey: 'AIzaSyA6znuTErJZHInnWhpOb3UD-XOuSJqe6_c',
  //         // apiKey: 'AIzaSyC2xpO5uZmaWu-7S7DEs5RUmiVYc4E2m-c',
  //         onSuggestionSelected: (selectedPlace) => _pickLocation(
  //             selectedPlace?.result.formattedAddress,
  //             '${selectedPlace?.result.geometry?.location.lat},${selectedPlace?.result.geometry?.location.lng}'),
  //         onNext: (GeocodingResult? result) => _pickLocation(
  //             result?.formattedAddress,
  //             '${result?.geometry.location.lat},${result?.geometry.location.lng}'),
  //       ));
  //   isLoading = false;
  // }

  // void _pickLocation(String? formattedAddress, String coordinates) async {
  //   if (formattedAddress == null) {
  //     isLoading = false;
  //     return;
  //   }
  //   log(formattedAddress, name: 'Picked Address');
  //   // infoController.address = formattedAddress;
  //   // infoController.coordinates = coordinates;
  //   // await infoController.savePersonalInfo(true);
  //   SharedPrefs.prefs.setString(
  //       SharedPrefsKeys.userStatus, AppConstants.readyToUseAppStatus);
  //   Navigator.push(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => const CreateAccountScreen(data: '')));
  //   // Get.offAllNamed(RouteNames.CREATE_ACCOUNT_SCREEN);
  // }

  void _getGoogleBusinessId() async {
    Get.dialog(
        WillPopScope(
          onWillPop: () async => false,
          child: const AlertDialog(
            content: LinearProgressIndicator(),
            title: Text('Loading Business...'),
          ),
        ),
        barrierDismissible: false);
    if (position == null) {
      await _determinePosition().then((pos) {
        // '${pos.}'
        if (pos != null) position = '${pos.latitude},${pos.longitude}';
      });
      await _determinePosition().then((pos) {
        if (pos != null) position = '${pos.latitude},${pos.longitude}';
      });

      if (position == null) {
        isSavingInfo = false;
        return;
      }
    }

    if (mounted) {
      final result = await showSearch<Results>(
          context: context, delegate: CustomSearchDelegate(position!));
      bool isBusinessIdExists =
          (_businessInfoController.businessInfo?.googleBusinessId ==
                  (result?.placeId ?? ""))
              ? false
              : await _businessInfoController
                  .checkIfBusinessIdExists(result?.placeId ?? "");

      Get.back();
      if (isBusinessIdExists) {
        Get.snackbar('Oops!', 'Business with this place id already exists');
        return;
      }
      if (result?.name != null) {
        _businessName.value = result!.name!;
        _businessInfoController.businessName = _businessName.value;
        _businessInfoController.businessCoordinates = _businessName.value;
      }
      _businessInfoController.rating = (result?.rating ?? 0.0).toString();
      _businessInfoController.types = result?.types ?? [];
      _businessInfoController.businessCoordinates =
          '${result?.geometry?.location?.lat ?? ''},${result?.geometry?.location?.lng ?? ''}';
      _businessInfoController.googleBusinessId.text = result?.placeId ?? "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
          padding: const EdgeInsets.only(bottom: 22, right: 10),
          child: InkWell(
              onTap: _saveDetails,
              // onTap: _registerUser,
              child: Image.asset(AppImages.halfProgress, height: 60))),
      // resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomBackButton(),
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 20),
                child: Text(
                  "${isOldUser ? "Edit" : "Fill"} this details",
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppConstants.poppinsFonts),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 55, left: 25, right: 125),
                child: Text(
                  AppConstants.createAccountDes,
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              // const SizedBox(height: 50),
              // CustomTextField(
              //     hintText: "First Name",
              //     textEditingController:
              //         _businessInfoController.firstNameController),
              // const SizedBox(height: 30),
              // CustomTextField(
              //     hintText: "last Name",
              //     textEditingController:
              //         _businessInfoController.lastNameController),
              // const SizedBox(height: 30),
              // Row(
              //   children: [
              //     CountryCodePicker(
              //         initialSelection:
              //             _businessInfoController.selectedCountryCode,
              //         onChanged: (value) => _businessInfoController
              //             .selectedCountryCode = value.dialCode,
              //         padding: const EdgeInsets.only(left: 15, right: 5)),
              //     Expanded(
              //       child: CustomTextField(
              //           margin: const EdgeInsets.only(right: 15),
              //           hintText: "Mobile Number",
              //           textEditingController:
              //               _businessInfoController.mobileController,
              //           textInputType: TextInputType.phone),
              //     ),
              //   ],
              // ),
              const SizedBox(height: 30),
              CustomTextField(
                hintText: "Google Business Id",
                isReadOnly: true,
                textEditingController: _businessInfoController.googleBusinessId,
                onTap: _getGoogleBusinessId,
                //  onTap: _determinePosition.th,
              ),
              Padding(
            padding: const EdgeInsets.only(left: 30, top: 20),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    // height: 179,
                    // width: 165,
                    decoration: BoxDecoration(
                      color: Colors.white,
                     
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 32,
                            offset: Offset.zero),
                      ],
                    ),
                   child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Selected Business Information'),
                      ),
                        Padding(
                padding: const EdgeInsets.only(left: 10, top: 5),
                child: Obx(() => _businessName.value.isEmpty
                    ? const SizedBox()
                    : Text("$_businessName",style: TextStyle(fontSize: 20,),)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 5),
                child: Text("Business Id: ${_businessInfoController.googleBusinessId.text}"),
              ),
                Padding(
                padding: const EdgeInsets.only(left: 15, top: 5),
                child: Text("Ratings: ${ _businessInfoController.rating}"),
              ),
               Padding(
                padding: const EdgeInsets.only(left: 15, top: 5),
                child: Text("Category: ${ _businessInfoController.types}",overflow: TextOverflow.ellipsis,),
              ),
              const SizedBox(height: 10,),
                    ],
                   ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
              
              ],
            ),
          ),
              // Padding(
              //   padding: const EdgeInsets.only(left: 30, top: 15),
              //   child: Obx(() => _businessName.value.isEmpty
              //       ? const SizedBox()
              //       : Text("Business Name: $_businessName")),
              // ),
              // Padding(
              //   padding: const EdgeInsets.only(left: 30, top: 15),
              //   child: Text("Business Place Id: ${_businessInfoController.googleBusinessId.text}"),
              // )
              // TextButton(
              //     onPressed: () {
              //       _addBusiness();
              //     },
              //     child: Text('data'))
            ],
          ),
        ),
      ),
    );
  }
}

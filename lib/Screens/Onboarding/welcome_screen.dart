import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:vendorapp/Models/sign_up.dart';
import 'package:vendorapp/Screens/Onboarding/create_account_screen.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/widget_animation.dart';
import '../../helper/helper_function.dart';
import '../../services/service_signup.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  GestureRecognizer? gestureRecognizer;
  bool loading = false;
  SignUp signUp = SignUp.init();
  // bool isLogin = false;

  // @override
  // void initState() {
  //   super.initState();
  // }

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  Future _signInWithGoogle() async {
    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();
    if (googleSignInAccount == null) {
      return false;
    }
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    final UserCredential authResult =
        await _auth.signInWithCredential(credential);

    var userEmail = authResult.user?.email;
    try {
      Map<String, dynamic> data =
          await ServiceCheckUser.checkUser(context, "${userEmail}");
      var business1 = data['data']['userData'];

      if (business1 != null) {
        HelperFunction.assignDataToPrefs(data['data'] ?? {}, isLogin: true);
        var business = data['data']['userData']['email'];

        if (business == userEmail) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CreateAccountScreen(data: "${userEmail}"),
            ),
          );
          // Timer(Duration(seconds: 2), () {
          //   Get.toNamed(RouteNames.BOTTOM_BAR_SCREEN);
          // });
        }
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CreateAccountScreen(data: "${userEmail}"),
          ),
        );
      }

      setState(() {
        loading = false;
      });
    } catch (e) {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(height: 100),
            Align(
                alignment: Alignment.center,
                child: Image.asset(
                  AppImages.knoffslogo,
                )),
            const Text(
              "Know nearby offers",
              style: TextStyle(
                  color: AppColors.darkGrey,
                  fontSize: 20,
                  fontWeight: FontWeight.w400),
            ),
            const SizedBox(
              height: 30,
            ),
            Align(
                alignment: Alignment.center,
                child: WidgetAnimation(
                  path: 'assets/images/welcome.json',
                  height: 240,
                )),
            const SizedBox(height: 80),
            InkWell(
              onTap: _signInWithGoogle,
              child: Container(
                width: 228,
                padding: const EdgeInsets.symmetric(vertical: 24),
                decoration: BoxDecoration(
                    color: AppColors.background,
                    border: Border.all(color: AppColors.darkText),
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromRGBO(90, 108, 234, 0.150),
                          offset: Offset(0, 26),
                          blurRadius: 35)
                    ]),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(AppImages.googleIcon, height: 37),
                    const SizedBox(width: 18),
                    const Text(
                      "Google",
                      style: TextStyle(
                          color: AppColors.darkText,
                          fontSize: 20,
                          fontWeight: FontWeight.w400),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLoginButton([void Function()? onTap]) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 60,
        margin: const EdgeInsets.symmetric(horizontal: 30),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(99),
            gradient: const LinearGradient(
                begin: Alignment(1, 0),
                end: Alignment(-1, 0),
                colors: [
                  Color.fromRGBO(146, 163, 253, 1),
                  Color.fromRGBO(157, 206, 255, 1)
                ])),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(AppImages.loginIcon, height: 25),
            const SizedBox(
              width: 10,
            ),
            const Text(
              "Login",
              style: TextStyle(
                  fontFamily: AppConstants.poppinsFonts,
                  color: AppColors.background,
                  fontSize: 16,
                  fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
    );
  }
}

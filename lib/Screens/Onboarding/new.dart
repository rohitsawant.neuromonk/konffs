import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Carousel1Widget extends StatefulWidget {
  @override
  _Carousel1WidgetState createState() => _Carousel1WidgetState();
}

class _Carousel1WidgetState extends State<Carousel1Widget> {
  @override
  Widget build(BuildContext context) {
    // Figma Flutter Generator Carousel1Widget - FRAME

    return Container(
        width: 375,
        height: 812,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40),
            topRight: Radius.circular(40),
            bottomLeft: Radius.circular(40),
            bottomRight: Radius.circular(40),
          ),
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
        child: Stack(children: <Widget>[
          Positioned(
              top: 94,
              left: 22,
              child: Container(
                  width: 332,
                  height: 565,
                  child: Stack(children: <Widget>[
                    Positioned(
                        top: 0,
                        left: 0,
                        child: Container(
                            width: 332,
                            height: 565,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.25),
                                    offset: Offset(0, 0),
                                    blurRadius: 4)
                              ],
                              color: Color.fromRGBO(255, 255, 255, 1),
                            ))),
                    Positioned(
                        top: 61.45411682128906,
                        left: 15,
                        child: Container(
                            width: 302,
                            height: 313.0124816894531,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/illustration_grow_business.png'),
                                  fit: BoxFit.fitWidth),
                            ))),
                    Positioned(
                        top: 409.9034729003906,
                        left: 26,
                        child: Text(
                          '"Explore Nearby Offers" is like your personal treasure map to finding super cool deals and special discounts that are just around the corner.',
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontFamily: 'Segoe UI',
                              fontSize: 16,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        )),
                  ]))),
          Positioned(
              top: 742,
              left: 78,
              child: Container(
                  width: 219,
                  height: 5,
                  child: Stack(children: <Widget>[
                    Positioned(
                      top: 0,
                      left: 0,
                      child: SvgPicture.asset('assets/images/client1.png',
                          semanticsLabel: 'line'),
                    ),
                    Positioned(
                      top: 0,
                      left: 77,
                      child: SvgPicture.asset('assets/images/line.svg',
                          semanticsLabel: 'line'),
                    ),
                    Positioned(
                      top: 0,
                      left: 154,
                      child: SvgPicture.asset('assets/images/line.svg',
                          semanticsLabel: 'line'),
                    ),
                  ]))),
        ]));
  }
}

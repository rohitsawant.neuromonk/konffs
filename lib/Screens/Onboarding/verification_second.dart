import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Widgets/custom_back_button.dart';
import '../../helper/iconstant.dart';

class VerificationSecond extends StatefulWidget {
  // final String data;

  const VerificationSecond({
    Key? key,
  }) : super(key: key);

  @override
  State<VerificationSecond> createState() => _VerificationSecondState();
}

class _VerificationSecondState extends State<VerificationSecond> {
  final TextEditingController emailController = TextEditingController();

  final otpController = TextEditingController();
  bool isLoading = false;
  String userName = "", email = "", password = "", userLastName = "";
  getInstance() async {
    await SharedPreferences.getInstance().then((value) {
      setState(() {
        userName = value.getString(IConstant.userName) as String;
        email = value.getString(IConstant.userEmailId) as String;
        // password = value.getString(IConstant.userIdd) as String;
        userLastName = value.getString(IConstant.userLastName) as String;
        // mobile = value.getString(IConstant.userMobileNo) as String;
        isLoading = false;
      });
    });
  }

  FirebaseAuth auth = FirebaseAuth.instance;
  void verifyUserPhoneNumber() {
    auth.verifyPhoneNumber(
      phoneNumber: "+919172268905",
      verificationFailed: (FirebaseAuthException e) {
        print(e.message);
      },
      codeSent: (String verificationId, int? resendToken) {},
      codeAutoRetrievalTimeout: (String verificationId) {
        print('TimeOut');
      },
      verificationCompleted: (PhoneAuthCredential phoneAuthCredential) {},
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
          padding: const EdgeInsets.only(bottom: 22, right: 10),
          child: InkWell(
              onTap: () {},
              child: Image.asset(AppImages.fullProgress, height: 60))),

      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomBackButton(),
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 20),
                child: Text(
                  "Verify Yourself",
                  style: const TextStyle(
                      color: AppColors.darkText,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppConstants.poppinsFonts),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 20, left: 25, right: 60),
                child: Text(
                  'You do not have any contact information registered with google. Please add it and come back. Thanks',
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              const SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Container(
                  height: 90,
                  width: 400,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromARGB(255, 165, 156, 156)
                              .withOpacity(0.1),
                          blurRadius: 32,
                          offset: Offset.zero),
                    ],
                  ),
                  // child: Row(
                  //   children: [
                  //     const SizedBox(
                  //       width: 20,
                  //     ),
                  //     Image.asset(
                  //       'assets/icons/Message.png',
                  //       height: 50,
                  //     ),
                  //     const SizedBox(
                  //       width: 20,
                  //     ),
                  //     Column(
                  //       crossAxisAlignment: CrossAxisAlignment.start,
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       children: [
                  //         Text(
                  //           'Via email:',
                  //           style: TextStyle(color: Colors.grey, fontSize: 16),
                  //         ),
                  //         Text(
                  //           '· · · ·  · · · ·  05',
                  //           style: TextStyle(
                  //               fontWeight: FontWeight.bold, fontSize: 16),
                  //         ),
                  //       ],
                  //     ),
                  //   ],
                  // ),
                ),
              ),
              // const SizedBox(height: 30),
              // Padding(
              //   padding: const EdgeInsets.only(left: 25, right: 25),
              //   child: Container(
              //     height: 90,
              //     width: 400,
              //     decoration: BoxDecoration(
              //       color: Colors.white,
              //       borderRadius: BorderRadius.circular(20),
              //       boxShadow: [
              //         BoxShadow(
              //             color: Color.fromARGB(255, 165, 156, 156)
              //                 .withOpacity(0.1),
              //             blurRadius: 32,
              //             offset: Offset.zero),
              //       ],
              //     ),
              //     child: Row(
              //       children: [
              //         const SizedBox(
              //           width: 20,
              //         ),
              //         Image.asset(
              //           'assets/icons/Email.png',
              //           height: 50,
              //         ),
              //         const SizedBox(
              //           width: 20,
              //         ),
              //         Column(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           children: [
              //             Text(
              //               'Via email:',
              //               style: TextStyle(fontSize: 16, color: Colors.grey),
              //             ),
              //             Row(
              //               children: [
              //                 Text(
              //                   '· · · · @gmail.com',
              //                   style: TextStyle(
              //                       fontWeight: FontWeight.bold, fontSize: 16),
              //                 ),
              //               ],
              //             ),
              //           ],
              //         ),
              //       ],
              //     ),
              //   ),
              // ),
              // const Padding(
              //   padding: EdgeInsets.only(top: 55, left: 25, right: 35),
              //   child: Text(
              //     'Note: If you do not hold any of this contact details then\nplease update it on google and come back later.',
              //     style: TextStyle(
              //         color: AppColors.darkText,
              //         fontSize: 12,
              //         fontWeight: FontWeight.w400),
              //   ),
              // ),
              // const SizedBox(
              //   height: 30,
              // ),
              // Center(
              //   child: SizedBox(
              //     width: 300,
              //     height: 80,
              //     child: CustomButton(
              //       label: 'Send OTP',
              //       onTap: () async {
              //         verifyUserPhoneNumber();
              //         setState(() {
              //           // visible = !visible;
              //         });
              //       },
              //     ),
              //   ),
              // ),
              // Center(
              //   child: SizedBox(
              //     width: 250,
              //     child: Padding(
              //       padding: const EdgeInsets.only(top: 20),
              //       child: Center(
              //         child: CustomTextField(
              //             hintText: "Enter OTP",
              //             textEditingController: otpController,
              //             textInputType: TextInputType.phone),
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

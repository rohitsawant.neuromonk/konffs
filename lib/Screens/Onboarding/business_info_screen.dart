
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Controllers/business_info_controller.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/custom_back_button.dart';
import '../../Widgets/custom_text_field.dart';

class BusinessInfoScreen extends StatefulWidget {
  const BusinessInfoScreen({Key? key}) : super(key: key);

  @override
  State<BusinessInfoScreen> createState() => _BusinessInfoScreenState();
}

class _BusinessInfoScreenState extends State<BusinessInfoScreen> {
  final _businessInfoController = Get.find<BusinessInfoController>();
  bool isOldUser = false;
  void _saveDetails() async {
    if (_businessInfoController.businessTitleController.text.isEmpty ||
        _businessInfoController.businessCategoryController.text.isEmpty ||
        _businessInfoController.businessContactController.text.isEmpty ||
        _businessInfoController.businessLocationController.text.isEmpty ||
        _businessInfoController.businessReviewController.text.isEmpty) {
      Get.snackbar("Oops!", "Any of the fields cannot be left empty");
      return;
    }

    bool isSuccess = await _businessInfoController.saveBusinessInfo();
    if (isSuccess) {
      isOldUser
          ? Get.offAllNamed(RouteNames.BOTTOM_BAR_SCREEN)
          : Get.toNamed(RouteNames.PAYMENT_SCREEN);
    }
  }

  @override
  void initState() {
    super.initState();

    isOldUser = _businessInfoController.businessInfo != null;

    if (isOldUser) {
      _businessInfoController.firstNameController.text =
          _businessInfoController.businessInfo?.firstName ?? "";
      _businessInfoController.lastNameController.text =
          _businessInfoController.businessInfo?.lastName ?? "";
      _businessInfoController.mobileController.text =
          _businessInfoController.businessInfo?.mobileNumber ?? "";
      // _businessInfoController.businessTitleController.text =
      //     _businessInfoController.businessInfo?.businessTitle ?? "";
      // _businessInfoController.businessCategoryController.text =
      //     _businessInfoController.businessInfo?.businessCategory ?? "";
      // _businessInfoController.businessContactController.text =
      //     _businessInfoController.businessInfo?.businessContact ?? "";
      // _businessInfoController.businessLocationController.text =
      //     _businessInfoController.businessInfo?.businessLocationLink ?? "";
      // _businessInfoController.businessReviewController.text =
      //     _businessInfoController.businessInfo?.businessReviewLink ?? "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const CustomBackButton(),
              const Padding(
                padding: EdgeInsets.only(left: 25, top: 20),
                child: Text(
                  "Business Information",
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                      fontFamily: AppConstants.poppinsFonts),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(top: 55, left: 25, right: 125),
                child: Text(
                  AppConstants.businessInfoDes,
                  style: TextStyle(
                      color: AppColors.darkText,
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
              ),
              const SizedBox(height: 20),
              CustomTextField(
                  hintText: "Business Title",
                  textEditingController:
                      _businessInfoController.businessTitleController),
              const SizedBox(height: 20),
              CustomTextField(
                  hintText: "Business Category",
                  textEditingController:
                      _businessInfoController.businessCategoryController),
              const SizedBox(height: 20),
              CustomTextField(
                  hintText: "Business Contact",
                  textEditingController:
                      _businessInfoController.businessContactController),
              const SizedBox(height: 20),
              CustomTextField(
                  hintText: "Business Google Location Link",
                  textEditingController:
                      _businessInfoController.businessLocationController),
              const SizedBox(height: 20),
              CustomTextField(
                  hintText: "Business Review Link",
                  textEditingController:
                      _businessInfoController.businessReviewController),
              // const Expanded(child: SizedBox()),
              SizedBox(height: MediaQuery.of(context).size.height * 0.13),
              Align(
                  alignment: const Alignment(.85, 0),
                  child: InkWell(
                      onTap: _saveDetails,
                      child: Image.asset(AppImages.halfProgress, height: 60))),
              const SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}

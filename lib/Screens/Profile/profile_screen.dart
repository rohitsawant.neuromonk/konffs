import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:vendorapp/Screens/Onboarding/create_account_screen.dart';

import '../../Controllers/business_info_controller.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Utils/shared_prefs.dart';
import '../../Widgets/account_delete_dialog.dart';
import '../../Widgets/action_box.dart';
import '../../Widgets/action_tile.dart';
import '../../Widgets/profile_detail_box.dart';
import '../../Widgets/profile_header.dart';
import '../../helper/helper_function.dart';
import '../../helper/iconstant.dart';

class ProfileScreen extends StatefulWidget {
  final void Function()? onBack;

  const ProfileScreen({Key? key, this.onBack}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool isLoading = false;
  String userName = "", email = "", password = "", userLastName = "";
  getInstance() async {
    await SharedPreferences.getInstance().then((value) {
      setState(() {
        userName = value.getString(IConstant.userName) as String;
        email = value.getString(IConstant.userEmailId) as String;
        // password = value.getString(IConstant.userIdd) as String;
        userLastName = value.getString(IConstant.userLastName) as String;
        // mobile = value.getString(IConstant.userMobileNo) as String;
        isLoading = false;
      });
    });
  }

  // Initialize
  @override
  void initState() {
    super.initState();
    getInstance();
  }

  final infoController = Get.find<BusinessInfoController>();
  bool isSigningOut = false;

  Future<void> _deleteAccount() async {
    bool cancel = false;

    await Get.dialog(
        WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: const Text("Are you sure want to delete account?"),
            actions: [
              TextButton(
                  onPressed: () {
                    cancel = true;
                    Get.back();
                  },
                  child: const Text("No")),
              TextButton(onPressed: () => Get.back(), child: const Text("Yes")),
            ],
          ),
        ),
        barrierDismissible: false);
    if (cancel) return;

    Get.dialog(const AccountDeleteDialog(), barrierDismissible: false);
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear().then((value) {
      Get.offAllNamed(RouteNames.WELCOME_SCREEN);
    });
    await infoController.deleteBusiness();
    await SharedPrefs.prefs.clear();
    Get.back();
    infoController.dispose();
  }

  Future<void> _signOut() async {
    isSigningOut = true;
    await GoogleSignIn().signOut();
    await FirebaseAuth.instance.signOut();

    await SharedPrefs.prefs.clear();
    Get.offAllNamed(RouteNames.WELCOME_SCREEN);
  }

  void _editProfile() {
    if (isSigningOut) {
      Get.snackbar("Error", "Please wait while we're signing you out!");
      return;
    }
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CreateAccountScreen(data: email)));
    // Get.toNamed(RouteNames.CREATE_ACCOUNT_SCREEN);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        children: [
          const SizedBox(height: 20),
          ProfileHeader(title: "Profile", onBack: widget.onBack),
          const SizedBox(height: 35),
          ProfileDetailBox(
            userName: "${userName}  ${userLastName}",
            offersViewed: (infoController.businessInfo?.totalOffersViewed ?? 0)
                .toString(),
            offersClaimed:
                (infoController.businessInfo?.totalOffersClaimed ?? 0)
                    .toString(),
            onEdit: _editProfile,
            email: email,
          ),
          // const SizedBox(height: 30),
          // const ActionBox(actionTiles: [
          //   // ActionTile(
          //   //   title: "Personal Data",
          //   //   img: AppImages.personOutlinedIcon,
          //   //   onTap: () => Get.toNamed(RouteNames.CREATE_ACCOUNT_SCREEN),
          //   // ),
          //   // ActionTile(img: AppImages.chartIcon, title: "Activity History")
          // ], label: "Account"),
          const SizedBox(height: 60),
          ActionBox(actionTiles: [
            ActionTile(
              img: AppImages.mailIcon,
              title: "Contact Us",
              onTap: () => launchUrlString("https:/rakapranav3@gmail.com"),
            ),
            ActionTile(
              img: AppImages.privacyPolicyIcon,
              title: "Privacy Policy",
              onTap: () => Get.toNamed(RouteNames.PRIVACY_POLICY_SCREEN),
            ),
            const ActionTile(img: AppImages.settingIcon, title: "Help"),
            ActionTile(
              img: AppImages.settingIcon,
              title: "Invite and Earn",
              onTap: () {},
              // => Get.toNamed(RouteNames.ABOUT_US_SCREEN),
            ),
            const ActionTile(img: AppImages.settingIcon, title: "Settings"),
            ActionTile(
              img: AppImages.personOutlinedIcon,
              title: "Logout",
              // onTap:()async{
              //   await logout();
              //   }
              //  _deleteAccount,

              onTap: _signOut,
            ),
            // ActionTile(
            //   img: AppImages.personOutlinedIcon,
            //   title: "Delete Account",
            //   onTap: _deleteAccount,
            // ),
          ], label: "Other")
        ],
      ),
    );
  }
}

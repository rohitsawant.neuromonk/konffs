class OfferList {
  int  isActive;
  String offerDetails, categoryType,id;
  int businessDetailsId;

String discount;
// bool?? b = null;
  String? offerStartDate, offerEndDate;
  OfferList({
    required this.id,
    required this.businessDetailsId,
    required this.isActive,
 
    // required this.minBuyCount,
    // required this.getFreeCount,
    required this.offerDetails,
    required this.categoryType,
    // required this.termsAndCondition,
    required this.discount,
    required this.offerStartDate,
    required this.offerEndDate,
  });
  static OfferList init() => OfferList(
      id: '',
      isActive: 0,
      // minBuyCount: 0,
      // getFreeCount: 0,
      offerDetails: '',
      categoryType: '',
      // termsAndCondition: '',
      discount: '',
      offerStartDate:'',
      offerEndDate:'',
       businessDetailsId: 0,);
}

import 'dart:math';
import 'dart:developer'as debug;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
import 'package:vendorapp/Widgets/widget_offer.dart';
import 'package:vendorapp/helper/helper_function.dart';
import 'package:vendorapp/services/service_signup.dart';
import '../../Controllers/business_info_controller.dart';
import '../../Controllers/offer_controller.dart';
import '../../Models/offer.dart';
import '../../Models/offer_model.dart';
import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Utils/route_names.dart';
import '../../Widgets/custom_search_bar.dart';
import '../../helper/iconstant.dart';

class HomeScreen extends StatefulWidget {
  final Offers? offers;
  const HomeScreen({
    Key? key,
    this.offers,
  }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final offerController = Get.put(OfferController());
  final RxList<Offer> _filteredOfferList = <Offer>[].obs;
  final BusinessInfoController businessInfoController =
      Get.find<BusinessInfoController>();

  bool loading = false;

  bool isLoading = false;
  String userName = "", email = "", password = "", userLastName = "";
  getInstance() async {
    await SharedPreferences.getInstance().then((value) {
      setState(() {
        userName = value.getString(IConstant.userName) as String;
        userLastName = value.getString(IConstant.userLastName) as String;
      });
    });
  }

  // Initialize
  @override
  void initState() {
    super.initState();
    getInstance();
    getOffers();
  }

  List<Offers> offers = [];
  void getOffers() async {
    int getBusineesId = await HelperFunction.getBusinessId();
    setState(() {
      loading = true;
    });
    var response = await ServiceOfferGet.getoffers(context, getBusineesId);
    var data = response['data'] ?? {};
    // debug.log("${data}");
    List thisList = data['offers'] ?? [];

    List<Offers> eventList = Offers.listFromMap(thisList);

    setState(() {
      offers = eventList;
      loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 18),
              child: _buildHeader(),
            ),
            _buildSearchBar(),
            const SizedBox(
              height: 10,
            ),
            isLoading
                ? const Center(child: CircularProgressIndicator())
                : offers.isEmpty
                    ? Center(child: Text('No...! Offers Available'))
                    : Column(
                        children: offers
                            .map((e) => OfferBoxAdd(
                                  offers: e,
                                ))
                            .toList())
          ],
        ),
      ),
    );
  }

  Widget _buildShimmerContainers() {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      child: ListView.builder(
        itemCount: 4,
        shrinkWrap: true,
        itemBuilder: (context, index) => Container(
          height: 95,
          margin: const EdgeInsets.symmetric(horizontal: 14, vertical: 10),
          decoration: BoxDecoration(
              color: AppColors.background,
              borderRadius: BorderRadius.circular(10)),
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Welcome Back,",
              style: TextStyle(
                  color: AppColors.darkGrey,
                  fontFamily: AppConstants.poppinsFonts,
                  fontWeight: FontWeight.w400,
                  fontSize: 12),
            ),
            const SizedBox(height: 5),
            Text(
              "${userName}  ${userLastName}",
              style: const TextStyle(
                  color: AppColors.darkText,
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  fontFamily: AppConstants.poppinsFonts),
            )
          ],
        ),
        GestureDetector(
          onTap: () => Get.toNamed(RouteNames.NOTIFICATION_SCREEN),
          child: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: AppColors.lightGrey.withOpacity(.5),
              borderRadius: BorderRadius.circular(8),
            ),
            alignment: Alignment.center,
            child: Image.asset(
              AppImages.notificationIcon,
              height: 18,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSearchBar() {
    return Row(
      children: [
        const SizedBox(width: 16),
        Expanded(
          child: CustomSearchBar(
            hintText: "Search Offers",
            onSearch: (keyword) {
              offers = [];
              if (keyword.isEmpty) {
                offers = offers;
                return;
              }

              for (Offer offer in offerController.offerList) {
                if ((offer.title ?? "")
                    .toLowerCase()
                    .contains(keyword.toLowerCase())) {
                  _filteredOfferList.add(offer);
                }
              }
            },
          ),
        ),
        const SizedBox(width: 16),
      ],
    );
  }
}

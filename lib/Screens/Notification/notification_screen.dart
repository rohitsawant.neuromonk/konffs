
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Utils/app_colors.dart';
import '../../Utils/app_constants.dart';
import '../../Utils/app_images.dart';
import '../../Widgets/profile_header.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(left: 30, top: 30),
            child: ProfileHeader(
              title: "Notification",
              onBack: () => Get.back(),
            ),
          ),
          const SizedBox(height: 20),
          ListView.separated(
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(horizontal: 25),
              itemBuilder: (context, index) => ListTile(
                    contentPadding: const EdgeInsets.symmetric(vertical: 8),
                    leading: CircleAvatar(
                      backgroundColor: AppColors.lightBlue.withOpacity(.5),
                      radius: 20,
                      child: Image.asset(AppImages.foodIcon, height: 17),
                    ),
                    title: const Text(
                      "Hey, it’s time for lunch",
                      style: TextStyle(
                          color: AppColors.darkText,
                          fontFamily: AppConstants.poppinsFonts,
                          fontWeight: FontWeight.w500,
                          fontSize: 12),
                    ),
                    subtitle: const Text("About 1 minutes ago",
                        style: TextStyle(
                            fontFamily: AppConstants.poppinsFonts,
                            fontWeight: FontWeight.w400,
                            fontSize: 10)),
                    trailing: const Icon(
                      Icons.more_vert,
                      size: 20,
                    ),
                  ),
              separatorBuilder: (context, index) => Container(
                    height: 1,
                    color: AppColors.darkGrey.withOpacity(.15),
                  ),
              itemCount: 6)
        ]),
      ),
    );
  }
}
